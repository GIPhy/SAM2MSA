/*
  ########################################################################################################

  SAM2MAP: inferring a consensus sequence from SAM-formatted read alignments
  
  Copyright (C) 2020-2023  Institut Pasteur

  This program is part of the package SAM2MSA.
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr
   Centre de Ressources Biologiques de l'Institut Pasteur (CRBIP)             research.pasteur.fr/en/b/VTq
   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr

  ########################################################################################################
*/

import java.io.*;
import java.util.*;

public class SAM2MAP {

    //### constants  ################################################################
    final static String VERSION = "0.4.231128c";
    final static String NOTHING = "N.o./.T.h.I.n.G";
    final static String STDIN = "-";
    final static int MIN_COV = 10;          // default minimum coverage
    final static int DEPTH = 150;           // y-axis size for drawing distributions
    final static String BLANK = "                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ";
    final static String[] NT_BIN = {"-A", "-AC", "-ACG", "-ACGT", "-ACT", "-AG", "-AGT", "-AT", "-C", "-CG", "-CGT", "-CT", "-G", "-GT", "-T", "AC", "ACG", "ACGT", "ACT", "AG", "AGT", "AT", "CG", "CGT", "CT", "GT"};
    final static String NT_DGNR = "amvnhrdwcsbygktMVNHRDWSBYK";

    //### io  #######################################################################
    static BufferedReader in;
    static BufferedWriter out;

    //### options  ##################################################################
    static File samFile;              // SAM file (mandatory option -i)
    static File refFile;              // reference file (mandatory option -r)
    static String basename;           // output basename for output files (mandatory option -o)
    static int mapQ;                  // minimum allowed MAPQ value to consider a read (option -Q; default: 20)
    static int minQ;                  // minimum allowed Phred score to consider a base (option -q; default 20)
    static char minPHRED;             // char associated to minQ
    static double pvC;                // p-value to determine the coverage lower/upper bounds (option -p; default: 0.02)
    static int minC;                  // coverage lower bound (option -c; default: estimated from data)
    static int maxC;                  // coverage upper bound (option -C; default: estimated from data)
    static int minS;                  // minimum number of reads per strand to consider a position (option -s; default: 5)
    static double bfreq;              // minimum frequency to infer a base (option -f; default: 0.8)
    static String mmc;                // allowing mismatches on particular positions (option -m; default "")
    static boolean mu;                // allowing mismatches on positions encoded 'u' (derived from mm; option -m)
    static boolean mO;                // allowing mismatches on positions encoded 'O' (derived from mm; option -m)
    static boolean mo;                // allowing mismatches on positions encoded 'o' (derived from mm; option -m)
    static boolean mS;                // allowing mismatches on positions encoded 'S' (derived from mm; option -m)
    static double pvSNP;              // p-value to determine the clonal SNP proportion (option -x; default: 0)
    static int ws;                    // window size to determine the clonal SNP proportion (option -w; default: 2000)
    static String pgname;             // outputting a unique (concatenated) pseudo-genome sequence with specified name (option -n)
    static boolean verb;              // verbose mode (option -v)

    //### data  #####################################################################
    static int k;                     // no. ref. sequence(s)
    static ArrayList<String> fhList;  // list of ref. FASTA header(s)
    static String[] sh;               // sorted list of ref. header(s)
    static String ref;                // (concatenated) ref. sequence(s)
    static int lref;                  // length of ref
    static int[] sr;                  // start indexes of ref. sequence(s) within ref
    static short[] oA, oC, oG, oT;    // no. occurences of ACGT per pos.
    static short[] og, or;            // no. occurences of gap and rev. per pos.
    static short[] oi;                // no. occurences of insertion events per pos.
    static short[] dpth;              // coverage depth per pos.
    static int nr;                    // no. selected reads
    static double arl;                // avg. read length
    static int[] ocd;                 // observed coverage distribution
    static double[] cNBzP;            // coverage Poisson+NB distribution parameters
    static StringBuilder pg;          // pseudo-genome
    static StringBuilder pc;          // character codes associated to pg
    static int nU;                    // no. positions encoded 'U'
    static int nu;                    // no. positions encoded 'u'
    static int nO;                    // no. positions encoded 'O'
    static int no;                    // no. positions encoded 'o'
    static int nS;                    // no. positions encoded 'S'
    static int nM;                    // no. positions encoded 'M'
    static int nD;                    // no. mismatch positions with inferred gap
    static int nm;                    // no. mismatch positions 
    static int nN;                    // no. mismatch positions with inferred degenerate nucleotide
    static int nX;                    // no. positions within biased SNP number
    static int[] mmd;                 // observed mismatch distribution
    static double[] sNB;              // SNP NB distribution parameters
    static int minSNP;                // no. SNP lower bound
    static double minpSNP;            // SNP proportion lower bound
    static int maxSNP;                // no. SNP upper bound
    static double maxpSNP;            // SNP proportion upper bound
   
    //### stuffs  ###################################################################
    static int c, i, l, o, r, u, v, x, y, z;
    static int end, flag, max, pos, ros, sumy, xmax;
    static double fx, cov, p, scale, sum, tmp, up, hmp, pp, rr;
    static boolean rev, mismatch;
    static char ch, co, cr;
    static String line, rd, ph;
    static StringBuilder sb;
    static short[] as;
    static char[] ca;
    static float[] fa;
    static String[] split;
    static ArrayList<String> alS;

    public static void main(String[] args) throws IOException {

	//##########################################################################################################
	//### man                                                                                                ###
	//##########################################################################################################
	if ( args.length < 2 ) {
	    System.out.println("");
	    System.out.println(" SAM2MAP v." + VERSION + "  Copyright (C) 2020  Institut Pasteur");
	    System.out.println("");
	    System.out.println(" SAM2MAP infers a consensus sequence from SAM-formatted read alignments against a");
	    System.out.println(" reference.  The inferred  consensus sequence  has always  the same  size as  the");
	    System.out.println(" reference one.  At each position,  the inferred character state is the majority-");
	    System.out.println(" rule one within the aligned bases (option -f).  For each position,  the inferred");
	    System.out.println(" character state is associated with one of the following map code:");
	    System.out.println("   U  under-covered position (options -p or -c)");
	    System.out.println("   u  position neighboring map code 'U'");
	    System.out.println("   O  over-covered position (options -p or -C)");
	    System.out.println("   o  position neighboring map code 'O'");
	    System.out.println("   S  strand-biased position (option -s)");
	    System.out.println("   X  position within SNP-rich or SNP-poor regions (options -x and -w)");
	    System.out.println("   M  unbiased position");
	    System.out.println(" Inferred  character states  associated  with the  map code  'U' are  always '?'.");
	    System.out.println(" Inferred character  states that  differ from  the reference ones  and associated");
	    System.out.println(" with the map codes  'u', 'O', 'o' or 'S'  can be replaced by 'X'  or not (option");
	    System.out.println(" -m). Inferred character states associated  with the map code 'X' are replaced by");
	    System.out.println(" 'x' (options -x and -w).");
	    System.out.println(" The main output file is  a map file that summarizes  the read alignments against");
	    System.out.println(" the reference sequence. The inferred sequence is also written in FASTA format.");
	    System.out.println("");
	    System.out.println(" USAGE: SAM2MAP [-i SAMFILE] [-r REFFILE] [-o BASENAME] ...");
	    System.out.println("");
	    System.out.println(" GENERAL OPTIONS:");
	    System.out.println("");
	    System.out.println("   -i FILE      read alignment in SAM format; set \"-\" to read from standard input");
	    System.out.println("                (mandatory)");
	    System.out.println("   -r FILE      reference sequence(s)  in FASTA format;  should contain  at least");
	    System.out.println("                one sequence used for the read alignment (mandatory)");
	    System.out.println("   -o BASENAME  basename for output files (mandatory)");
	    System.out.println("   -n STRING    name of the inferred sequence;  when set,  a unique sequence will");
	    System.out.println("                be written in a FASTA file with the specified name in the header");
	    System.out.println("   -v           verbose mode");
	    System.out.println("");
	    System.out.println(" READ ALIGNMENT:");
	    System.out.println("");
	    System.out.println("   -q INTEGER   minimum allowed  Phred score;  sequenced bases  associated with a");
	    System.out.println("                Phred  score  smaller  than  the   specified  threshold  are  not");
	    System.out.println("                considered (default: 20)");
	    System.out.println("   -Q INTEGER   minimum allowed  mapping Phred  score;  aligned reads  associated");
	    System.out.println("                with a Phred score  smaller than the specified  threshold are not");
	    System.out.println("                considered (default: 20)");
	    System.out.println("");
	    System.out.println(" READ COVERAGE:");
	    System.out.println("");
	    System.out.println("   -p NUMBER    p-value  to  determine the  coverage  depth  confidence interval;");
	    System.out.println("                after  estimating  a  theoretical   coverage  depth  distribution");
	    System.out.println("                (Poisson  + NB)  from the  read alignments,  the lower  and upper");
	    System.out.println("                coverage bounds  are determined  by the  CDF NB values p and 1-p,");
	    System.out.println("                respectively (default: 0.005)");
	    System.out.println("   -c INTEGER   coverage depth  lower bound;  if the  number of  aligned reads is");
	    System.out.println("                smaller  than  this  threshold,  the  corresponding  position  is");
	    System.out.println("                considered as  under-covered and associated with the map code 'U'");
	    System.out.println("                (default: estimated from the data via option -p, but at least 10)");
	    System.out.println("   -C INTEGER   coverage depth  upper bound;  if the  number of  aligned reads is");
	    System.out.println("                larger  than  this  threshold,   the  corresponding  position  is");
	    System.out.println("                considered as over-covered  and associated with the  map code 'O'");
	    System.out.println("                (default: estimated from the data via option -p)");
	    System.out.println("   -s INTEGER   minimum number of  reads for each strand  to trust a position; if");
	    System.out.println("                a position  does not verify  this condition,  it is considered as");
	    System.out.println("                strand-biased and associated with the map code 'S' (default: 5)");
	    System.out.println("");
	    System.out.println(" CHARACTER STATE INFERENCE:");
	    System.out.println("");
	    System.out.println("   -f NUMBER    minimum proportion  to infer  the majority-rule  character state;");
	    System.out.println("                at each position,  the set of  most occuring aligned  nucleotides");
	    System.out.println("                is built  up to  this threshold  and the  corresponding character");
	    System.out.println("                state is inferred from the one(s) in this set (default: 0.8)");
	    System.out.println("   -m STRING    allowing mismatch in biased positions; when an inferred character");
	    System.out.println("                state differs  from the  reference one  and is  associated to map");
	    System.out.println("                codes  'u',  'O',  'o' or  'S', it is replaced by 'X' by default;");
	    System.out.println("                this can  be relaxed  by specifying  the  map  code(s) for  which");
	    System.out.println("                mismatches are allowed (default: \"M\")");
	    System.out.println("");
	    System.out.println(" SNP DENSITY:");
	    System.out.println("");
	    System.out.println("   -x NUMBER    threshold to determine  whether a position  does not belong  to a");
	    System.out.println("                clonal SNP  density region;  after estimating  a theoretical  SNP");
	    System.out.println("                density distribution  (NB) using  a sliding  window  (option -w),");
	    System.out.println("                each position is associated  to an index that  is close to 0 when");
	    System.out.println("                it belongs to  a region containing  significantly too few  or too");
	    System.out.println("                many SNP;  every  position  with index  lower than  the specified");
	    System.out.println("                threshold (e.g. 0.01) is associated to map codes 'X' and replaced");
	    System.out.println("                by 'x' (default: 0)");
	    System.out.println("   -w INTEGER   window size to assess clonal SNP density (default: 1000)");
	    System.out.println("");

	    System.exit(0);
	}


	//##########################################################################################################
	//### reading options                                                                                    ###
	//##########################################################################################################
	samFile = new File(NOTHING); // option -i
	refFile = new File(NOTHING); // option -r
	basename = NOTHING;          // option -o
	mapQ = 20;                   // option -Q 
	minQ = 20;                   // option -q 
	minC = -1;                   // option -c
	maxC = -1;                   // option -C
	minS = 5;                    // option -s
	pvC = 0.005;                 // option -p
	bfreq = 0.8;                 // option -f
	mmc = "";                    // option -m
	pvSNP = 0.0;                 // option -x
	ws = 1000;                   // option -w
	pgname = NOTHING;            // option -n
	verb = false;                // option -v
	o = -1;
	while ( ++o < args.length ) {
	    if ( args[o].equals("-i") )     { samFile = new File(args[++o]);         continue; }
	    if ( args[o].equals("-r") )     { refFile = new File(args[++o]);         continue; }
	    if ( args[o].equals("-o") )     { basename = args[++o];                  continue; }
	    if ( args[o].equals("-q") ) try { minQ = Integer.parseInt(args[++o]);    continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -q)"); System.exit(1); }
	    if ( args[o].equals("-Q") ) try { mapQ = Integer.parseInt(args[++o]);    continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -Q)"); System.exit(1); }
	    if ( args[o].equals("-c") ) try { minC = Integer.parseInt(args[++o]);    continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -c)"); System.exit(1); }
	    if ( args[o].equals("-C") ) try { maxC = Integer.parseInt(args[++o]);    continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -C)"); System.exit(1); }
	    if ( args[o].equals("-p") ) try { pvC = Double.parseDouble(args[++o]);   continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -p)"); System.exit(1); }
	    if ( args[o].equals("-s") ) try { minS = Integer.parseInt(args[++o]);    continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -s)"); System.exit(1); }
	    if ( args[o].equals("-f") ) try { bfreq = Double.parseDouble(args[++o]); continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -f)"); System.exit(1); }
	    if ( args[o].equals("-m") )     { mmc = args[++o];                       continue; }
	    if ( args[o].equals("-x") ) try { pvSNP = Double.parseDouble(args[++o]); continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -x)"); System.exit(1); }
	    if ( args[o].equals("-w") ) try { ws = Integer.parseInt(args[++o]);      continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -w)"); System.exit(1); }
	    if ( args[o].equals("-n") )     { pgname = args[++o];                    continue; }
	    if ( args[o].equals("-v") )     { verb = true;                           continue; }
	}
	//### testing mandatory options -i, -r and -b
	if ( samFile.toString().equals(NOTHING) )                       { System.err.println("SAM-formatted file not specified (option -i)");             System.exit(1); }
	if ( ! samFile.exists() && ! samFile.toString().equals(STDIN) ) { System.err.println("SAM-formatted file does not exist (option -i)");            System.exit(1); }
	if ( refFile.toString().equals(NOTHING) )                       { System.err.println("FASTA-formatted reference file not specified (option -r)"); System.exit(1); }
	if ( basename.equals(NOTHING) )                                 { System.err.println("output file basename not specified (option -o)");           System.exit(1); }
	//### testing numerical options
	if ( bfreq <= 0 || bfreq >= 1 )                                 { System.err.println("incorrect proportion value: " + bfreq + " (option -f)");    System.exit(1); }
	//if ( minS < 0 )                                                 { System.err.println("incorrect positive value: " + minS + " (option -s)");       System.exit(1); }
	if ( pvC < 0 || pvC > 0.5 )                                     { System.err.println("incorrect p-value: " + pvC + " (option -p)");               System.exit(1); }
	if ( pvSNP < 0 || pvSNP > 0.5 )                                 { System.err.println("incorrect p-value: " + pvSNP + " (option -x)");             System.exit(1); }
	if ( ws < 100 )                                                 { System.err.println("window size too small: " + ws + " (option -w)");            System.exit(1); }
	if ( ws > 32000 )                                               { System.err.println("window size too large: " + ws + " (option -w)");            System.exit(1); }
	//### setting minPHRED from minQ
	minPHRED = (char) (33 + minQ);
	//### reading special mismatch codes
	mu = mO = mo = mS = false; c = mmc.length();
	while ( --c >= 0 )
	    switch ( mmc.charAt(c) ) {
	    case 'u': mu = true; continue;
	    case 'O': mO = true; continue;
	    case 'o': mo = true; continue;
	    case 'S': mS = true; continue;
	    }
	
	
	//##########################################################################################################
	//### reading refFile and storing reference sequence(s)                                                  ###
	//##########################################################################################################
	//### reading refFile  ######################################################
	if ( verb ) {
	    System.err.println("Reading " + refFile.toString() + " ...");
	}
	fhList = new ArrayList<String>(); alS = new ArrayList<String>(); sb = new StringBuilder(""); max = 0;
	in = new BufferedReader(new FileReader(refFile));
	while ( true ) {
	    try { line = in.readLine().trim(); } catch ( NullPointerException e ) { alS.add(sb.toString()); in.close();	break; }
	    if ( line.length() == 0 ) continue;
	    if ( line.charAt(0) == '>' ) {
		if ( sb.length() != 0 ) {
		    alS.add(sb.toString());
		    sb = new StringBuilder("");
		}		
		line = line.substring(1).trim();
		if ( (i=line.indexOf(" ")) != -1 ) line = line.substring(0, i);
		fhList.add(line);
		max = ( max < (x=line.length()) ) ? x : max;
		continue;
	    }
	    sb = sb.append(line);
	}
	k = fhList.size(); 
	if ( verb ) {
	    r = -1; while ( ++r < k ) System.err.println("  " + (fhList.get(r)+BLANK).substring(0,max) + "  " + alS.get(r).length() + " bps");
	}
	//### sorting reference sequence(s)  ########################################
	sh = new String[k]; sh = fhList.toArray(sh); Arrays.sort(sh);
	sr = new int[k]; r = -1; sb = new StringBuilder("");
	for (String h: sh) { sr[++r] = sb.length(); sb = sb.append(alS.get(fhList.indexOf(h))); }
	ref = sb.toString().toUpperCase();
	lref = ref.length();
	//## NOTE:  pos. p  in  fhList.get(i)  corresponds  to  pos. sr[r]+p  in  ref  where  r = Arrays.binarySearch(sh, fhList.get(i))
	alS = null;
	if ( verb ) {
	    System.err.println("no. sequences             " + k);
	    System.err.println("no. bases                 " + lref);
	    System.err.println("");
	}
	
	
	//##########################################################################################################
	//### reading samFile                                                                                    ###
	//##########################################################################################################
	if ( verb ) {
	    System.err.println("Reading " + samFile.toString() + " ...");
	}
	oA = new short[lref]; oC = new short[lref]; oG = new short[lref]; oT = new short[lref]; og = new short[lref]; or = new short[lref]; oi = new short[lref]; dpth = new short[lref];
	in = ( samFile.toString().equals(STDIN) ) ? new BufferedReader(new InputStreamReader(System.in)) : new BufferedReader(new FileReader(samFile));
	try { while ( (line=in.readLine().trim()).length() == 0 || line.charAt(0) == '@' ) {} }
	catch ( NullPointerException e ) { System.out.println("empty SAM file: " + samFile.toString() + " (option -i)"); System.exit(1); }
	arl = nr = 0;
	while ( true ) {
	    //try { split = in.readLine().trim().split("\\t"); } catch ( NullPointerException e ) { in.close(); break; }
	    try { split = samSplit(in.readLine().trim()); } catch ( NullPointerException e ) { in.close(); break; }
	    
	    //### reading SAM tabulated line  #######################################
	    if ( ((flag=Integer.parseInt(split[1])) & 4) != 0             //## NOTE: bit 2 in FLAG is set  => unmapped read   => continue
		 || Integer.parseInt(split[4]) <= mapQ                    //## NOTE: MAPQ value is lower than the cutoff mapQ => continue
		 || (r=Arrays.binarySearch(sh, split[2])) < 0 ) continue; //## NOTE: current read is aligned on sh[r]

	    pos = Integer.parseInt(split[3]); pos += sr[r];               //## NOTE: current read alignment starts at position pos in ref
	    rev = ( (flag & 16) != 0 );                                   //## NOTE: bit 4 in FLAG is set => read mapped on the reverse strand
	    rd = split[9]; ph = split[10];                                //## NOTE: rd & ph => current read & phred scores
	    split = split[5].split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");    //## NOTE: last split is the CIGAR content

	    ++nr;
	    
	    //### updating map data  ################################################
	    --pos; --pos;     //## NOTE: pos: position within ref
	    ros = -1;         //## NOTE: pos: position within rd
	    l = split.length; c = -1;
	    //## NOTE: rev alignment
	    if ( rev ) {
		while ( (c+=2) < l ) {
		    x = Integer.parseInt(split[--c]);
		    switch ( split[++c].charAt(0) ) {
		    case 'M': case 'X': case '=':
			arl += x;
			while ( --x >= 0 ) {
			    switch ( (ph.charAt(++ros) <= minPHRED) ? '@' : rd.charAt(ros) ) {
			    case 'a': case 'A': ++oA[++pos]; ++dpth[pos]; ++or[pos]; continue;
			    case 'c': case 'C': ++oC[++pos]; ++dpth[pos]; ++or[pos]; continue;
			    case 'g': case 'G': ++oG[++pos]; ++dpth[pos]; ++or[pos]; continue;
			    case 't': case 'T': ++oT[++pos]; ++dpth[pos]; ++or[pos]; continue;
			    default:            ++pos;                               continue;
			    }
			}
			continue;
		    case 'D': while ( --x >= 0 ) { ++og[++pos]; ++or[pos]; ++dpth[pos]; } continue;
		    case 'N':                  pos += x; arl += x;                        continue;
		    case 'I': if ( c < l-1 ) { ros += x; arl += x; ++oi[++pos]; --pos; }  continue; // in some rare cases, the CIGAR ends with an Insertion event
		    case 'S':                  ros += x; arl += x;                        continue;
		    default:  c = l;                                                      continue;
		    }
		}
		continue;
	    }
	    //## NOTE: fwd alignment
	    while ( (c+=2) < l ) {
		x = Integer.parseInt(split[--c]);
		switch ( split[++c].charAt(0) ) {
		case 'M': case 'X': case '=':
		    arl += x;
		    while ( --x >= 0 ) {
			switch ( (ph.charAt(++ros) <= minPHRED) ? '@' : rd.charAt(ros) ) {
			case 'a': case 'A': ++oA[++pos]; ++dpth[pos]; continue;
			case 'c': case 'C': ++oC[++pos]; ++dpth[pos]; continue;
			case 'g': case 'G': ++oG[++pos]; ++dpth[pos]; continue;
			case 't': case 'T': ++oT[++pos]; ++dpth[pos]; continue;
			default:            ++pos;                    continue;
			}
		    }
		    continue;
		case 'D': while ( --x >= 0 ) { ++og[++pos]; ++dpth[pos]; }           continue;
		case 'N':                  pos += x; arl += x;                       continue;
		case 'I': if ( c < l-1 ) { ros += x; arl += x; ++oi[++pos]; --pos; } continue; // in some rare cases, the CIGAR ends with an Insertion event
		case 'S':                  ros += x; arl += x;                       continue;
		default:  c = l;                                                     continue;
		}
	    }
	}
	/*
	pos = -1;
	while ( ++pos < lref )
	    if ( oi[pos] > 5 ) System.out.println("WARNING: " + oi[pos] + " aligned reads with insertion to the reference at position " + (pos+1));
	*/
	    
	
	//##########################################################################################################
	//### estimating coverage statistics                                                                     ###
	//##########################################################################################################
	//### average read length, no. mapped position, and highest coverage depth ##
	arl /= nr;
	if ( verb ) {
	    System.err.println("no. selected reads        " + nr);
	    System.err.println("avg. read length          " + String.format(Locale.US, "%.0f", arl) + " bps");
	}
	l = xmax = 0;
	for (short d: dpth) {
	    x = ( (x=d) < 0 ) ? -x : x;
	    l += ( x != 0 ) ? 1 : 0;
	    xmax = ( xmax < x ) ? x : xmax;
	}
	if ( verb ) {
	    System.err.println("no. mapped positions      " + l);
	    System.err.println("avg. coverage             " + ((int)(arl*nr/l)) + " reads per pos.");
	}
	//### fitting Poisson+NB theo. distribution on the observed one  ############
	if ( verb ) {
	    System.err.println("");
	    System.err.println("estimating coverage statistics...");
	}
	ocd = new int[++xmax];
	for (short d: dpth) ++ocd[(((x=d)<0)?-x:x)];
	cNBzP = fitPtNB(ocd);
	//### setting minC and maxC  ################################################
	x = cdfNBinv(pvC, cNBzP[2], cNBzP[3]);
	if ( minC < 0 ) minC = (x < MIN_COV) ? MIN_COV : x;
	y = cdfNBinv(1-pvC, cNBzP[2], cNBzP[3]);
	if ( maxC < 0 ) maxC = y;
	if ( verb ) { //                                                                         ? Poisson  :                ? GP                    : NB
	    System.err.println("mean coverage             " + ((int)(Double.isInfinite(cNBzP[3]) ? cNBzP[2] : (cNBzP[3] < 0) ? cNBzP[2]/(1-cNBzP[3]) : cNBzP[2]*cNBzP[3]/(1-cNBzP[2]))) + "x");
	    System.err.println("coverage " + String.format(Locale.US, "%.2f", 100.0*(1.0-2.0*pvC)) + "% CI        ["  + x + " , " + y + "]");
	    System.err.println("coverage lower bound (U)  " + minC + "x");
	    System.err.println("coverage upper bound (O)  " + maxC + "x");
	}
	//### writing cov.txt file  #################################################
	writeCov(new File(basename + ".cov.txt"), ocd, cNBzP, lref, minC, maxC, DEPTH);
	if ( verb ) {
	    System.err.println("coverage distribution:    " + basename + ".cov.txt");
	}

	
	//##########################################################################################################
	//### estimating pseudo-genome                                                                           ###
	//##########################################################################################################
	//### inferring base for each ref position  #################################
	pg = new StringBuilder(ref); pc = new StringBuilder(ref); pos = lref;
	while ( --pos >= 0 ) {
	    ch = '?';  //##  NOTE: ch = inferred character state at position pos
	    co = 'U';  //##  NOTE: co = mapping code at position pos
	    if ( (cov=dpth[pos]) >= minC ) {
		//## computing ch
		max = 0;
		if ( (o=oA[pos]) > max ) { max = o; ch = 'A'; }
		if ( (o=oC[pos]) > max ) { max = o; ch = 'C'; }
		if ( (o=oG[pos]) > max ) { max = o; ch = 'G'; }
		if ( (o=oT[pos]) > max ) { max = o; ch = 'T'; }
		if ( (o=og[pos]) > max ) { max = o; ch = '-'; }
		if ( max / cov < bfreq ) {  //## NOTE: ch is a degenerate character state
		    split = new String[5];
		    split[0] = String.format("%09d", oA[pos]) + "A";
		    split[1] = String.format("%09d", oC[pos]) + "C";
		    split[2] = String.format("%09d", oG[pos]) + "G";
		    split[3] = String.format("%09d", oT[pos]) + "T";
		    split[4] = String.format("%09d", og[pos]) + "-";
		    Arrays.sort(split);
		    sum = 0; line = ""; x = 5;
		    do { sum += Integer.parseInt(split[--x].substring(0,9)); line = line + split[x].charAt(9); } while ( sum / cov < bfreq );
		    ca = line.toCharArray();
		    Arrays.sort(ca);
		    ch = ( (x=Arrays.binarySearch(NT_BIN, new String(ca))) < 0 ) ? 'x' : NT_DGNR.charAt(x);
		}
		//## computing co
		co = (cov > maxC) ? 'O' : ((o=or[pos]) <= minS || cov-o <= minS) ? 'S' : 'M';
	    }
	    pg.setCharAt(pos, ch); //## NOTE: inferred character state at pos
	    pc.setCharAt(pos, co); //## NOTE: map code at pos
	}
	//### setting neighbors of 'O' and 'U' positions  ###########################
	arl /= 2;
	r = -1;
	while ( ++r < k ) {
	    //### encoding 'o'  #####################################################
	    pos = sr[r]; end = ( ++r == k ) ? lref : sr[r]; --r; --pos; 
	    do {
		pos = pc.indexOf("O", ++pos); pos = ( pos < 0 ) ? end : pos; //while ( (++pos < end) && (pc.charAt(pos) != 'O') ) {}
		if ( pos < end ) { c = ((x=pos-(int)arl) > 0) ? x : -1;      while ( ++c < pos )  if ( ((co=pc.charAt(c)) != 'O') && (co != 'U') && (co != 'S') ) pc.setCharAt(c, 'o'); }
		while ( (++pos < end) && (pc.charAt(pos) == 'O') ) {}
		if ( pos < end ) { c = ((x=pos+(int)arl) < lref) ? x : lref; while ( --c >= pos ) if ( ((co=pc.charAt(c)) != 'O') && (co != 'U') && (co != 'S') ) pc.setCharAt(c, 'o'); }
	    } while ( pos < end );
	    //## encoding 'u'  ######################################################
	    pos = sr[r]; end = ( ++r == k ) ? lref : sr[r]; --r; --pos; 
	    do {
		pos = pc.indexOf("U", ++pos); pos = ( pos < 0 ) ? end : pos; //while ( (++pos < end) && (pc.charAt(pos) != 'U') ) {}
		if ( pos < end ) { c = ((x=pos-(int)arl) > 0) ? x : -1;      while ( ++c < pos )  if ( ((co=pc.charAt(c)) != 'O') && (co != 'U') && (co != 'S') ) pc.setCharAt(c, 'u'); }
		while ( (++pos < end) && (pc.charAt(pos) == 'U') ) {}
		if ( pos < end ) { c = ((x=pos+(int)arl) < lref) ? x : lref; while ( --c >= pos ) if ( ((co=pc.charAt(c)) != 'O') && (co != 'U') && (co != 'S') ) pc.setCharAt(c, 'u'); }
	    } while ( pos < end );
	}
	arl *= 2;
	//## discarding mismatches at positions encoded as 'u', 'O', 'o' or 'S'  ###
	if ( mu || mO || mo || mS ) {
	    pos = lref;
	    while ( --pos >= 0 )
		switch ( pc.charAt(pos) ) {
		case 'M': case 'U':                                                                                                                continue;
		case 'u': if ( (! mu) && Character.toUpperCase(pg.charAt(pos)) != Character.toUpperCase(ref.charAt(pos)) ) pg.setCharAt(pos, 'X'); continue;
		case 'O': if ( (! mO) && Character.toUpperCase(pg.charAt(pos)) != Character.toUpperCase(ref.charAt(pos)) ) pg.setCharAt(pos, 'X'); continue;
		case 'o': if ( (! mo) && Character.toUpperCase(pg.charAt(pos)) != Character.toUpperCase(ref.charAt(pos)) ) pg.setCharAt(pos, 'X'); continue;
		case 'S': if ( (! mS) && Character.toUpperCase(pg.charAt(pos)) != Character.toUpperCase(ref.charAt(pos)) ) pg.setCharAt(pos, 'X'); continue;
		}
	}


	//##########################################################################################################
	//### estimating SNP statistics                                                                          ###
	//##########################################################################################################
	if ( pvSNP > 0 ) {
	    if ( verb ) {
		System.err.println("");
		System.err.println("estimating SNP statistics...");
	    }
	    //### running sliding window  ###############################################
	    mmd = getSNPd(ws, 0.95, ref, pg, sr);
	    //### fitting NB theo. distribution on the observed one  ####################
	    sNB = fitNB(mmd);
	    //### setting minSNP and maxSNP  ############################################
	    p = 0.005;
	    minSNP = cdfNBinv(p, sNB[0], sNB[1]);   minpSNP = minSNP / (double) ws;   
	    maxSNP = cdfNBinv(1-p, sNB[0], sNB[1]); maxpSNP = maxSNP / (double) ws; 
	    if ( verb ) {
		System.err.println("sliding window size       " + ws);
		System.err.println("clonal SNP prop. " + String.format(Locale.US, "%.0f", 100.0*(1.0-2.0*p)) + "% CI   "  + String.format(Locale.US, "[%.5f , %.5f]", minpSNP, maxpSNP));
		System.err.println(String.format(Locale.US, "mean clonal p-distance    %.8f", sNB[0]*sNB[1]/(ws*(1-sNB[0]))));
	    }
	    //### writing snp.txt file  #################################################
	    writeSnp(new File(basename + ".snp.txt"), mmd, sNB, ws, minSNP, maxSNP, DEPTH);
	    if ( verb ) {
		System.err.println("SNP distribution:         " + basename + ".snp.txt");
	    }
	    //### searching for biased SNP regions  #####################################
	    fa = new float[lref]; Arrays.fill(fa, (float) 0.1);
	    pp = sNB[0]; rr = sNB[1] / ws;
	    r = -1;
	    while ( ++r < k ) {
		pos = sr[r]; end = ( ++r == k ) ? lref : sr[r]; --r; --pos;
		//## short segments, i.e. shorter than ws
		if ( end - pos < ws ) {
		    x = y = 0;
		    while ( ++pos < end )
			switch ( ref.charAt(pos) ) {
			case 'A': case 'a':
			    switch ( pg.charAt(pos) ) { case 'A': case 'a': case 'N': case 'n': ++y; continue;  case 'C': case 'c': case 'G': case 'g': case 'T': case 't': ++x; ++y; continue;  default: continue; } 
			case 'C': case 'c':
			    switch ( pg.charAt(pos) ) { case 'C': case 'c': case 'N': case 'n': ++y; continue;  case 'A': case 'a': case 'G': case 'g': case 'T': case 't': ++x; ++y; continue;  default: continue; }
			case 'G': case 'g':
			    switch ( pg.charAt(pos) ) { case 'G': case 'g': case 'N': case 'n': ++y; continue;  case 'A': case 'a': case 'C': case 'c': case 'T': case 't': ++x; ++y; continue;  default: continue; }
			case 'T': case 't':
			    switch ( pg.charAt(pos) ) { case 'T': case 't': case 'N': case 'n': ++y; continue;  case 'A': case 'a': case 'C': case 'c': case 'G': case 'g': ++x; ++y; continue;  default: continue; }
			case 'N': case 'n': case 'X': case 'x':
			    switch ( pg.charAt(pos) ) { case '?': continue;  default: ++y; continue; }
			}
		    if ( y > 0 ) { 
			p = cdfNB(x, pp, rr * y);                                  //## NOTE: Pr(X <= x) with X~NB
			if ( x > 0 ) p = Math.min(p , 1 - cdfNB(x-1, pp, rr * y)); //## NOTE: Pr(X >= x) with X~NB
			c = sr[r]; --c;
			if ( p < pvSNP ) while ( ++c < end ) if ( pc.charAt(c) != 'U' ) pc.setCharAt(c, 'X');
		    }		    
		    continue;
		}
		//## large segments, i.e. larger than ws
		l = pos + ws;
		x = y = 0;
		while ( ++pos < l ) 
		    switch ( ref.charAt(pos) ) {
		    case 'A': case 'a':
			switch ( pg.charAt(pos) ) { case 'A': case 'a': case 'N': case 'n': ++y; continue;  case 'C': case 'c': case 'G': case 'g': case 'T': case 't': ++x; ++y; continue;  default: continue; } 
		    case 'C': case 'c':
			switch ( pg.charAt(pos) ) { case 'C': case 'c': case 'N': case 'n': ++y; continue;  case 'A': case 'a': case 'G': case 'g': case 'T': case 't': ++x; ++y; continue;  default: continue; }
		    case 'G': case 'g':
			switch ( pg.charAt(pos) ) { case 'G': case 'g': case 'N': case 'n': ++y; continue;  case 'A': case 'a': case 'C': case 'c': case 'T': case 't': ++x; ++y; continue;  default: continue; }
		    case 'T': case 't':
			switch ( pg.charAt(pos) ) { case 'T': case 't': case 'N': case 'n': ++y; continue;  case 'A': case 'a': case 'C': case 'c': case 'G': case 'g': ++x; ++y; continue;  default: continue; }
		    case 'N': case 'n': case 'X': case 'x':
			switch ( pg.charAt(pos) ) { case '?': continue;  default: ++y; continue; }
		    }
		ros = sr[r]; --ros; --pos; 
		while ( ++pos < end ) {
		    switch ( ref.charAt(pos) ) {
		    case 'A': case 'a':
			switch ( pg.charAt(pos) ) { case 'A': case 'a': case 'N': case 'n': ++y; break;  case 'C': case 'c': case 'G': case 'g': case 'T': case 't': ++x; ++y; break;  default: break; } break;
		    case 'C': case 'c':
			switch ( pg.charAt(pos) ) { case 'C': case 'c': case 'N': case 'n': ++y; break;  case 'A': case 'a': case 'G': case 'g': case 'T': case 't': ++x; ++y; break;  default: break; } break;
		    case 'G': case 'g':
			switch ( pg.charAt(pos) ) { case 'G': case 'g': case 'N': case 'n': ++y; break;  case 'A': case 'a': case 'C': case 'c': case 'T': case 't': ++x; ++y; break;  default: break; } break;
		    case 'T': case 't':
			switch ( pg.charAt(pos) ) { case 'T': case 't': case 'N': case 'n': ++y; break;  case 'A': case 'a': case 'C': case 'c': case 'G': case 'g': ++x; ++y; break;  default: break; } break;
		    case 'N': case 'n': case 'X': case 'x':
			switch ( pg.charAt(pos) ) { case '?': break;  default: ++y; break; } break;
		    }
		    if ( y > 0 ) {
			p = cdfNB(x, pp, rr * y);                                  //## NOTE: Pr(X <= x) with X~NB
			if ( x > 0 ) p = Math.min(p , 1 - cdfNB(x-1, pp, rr * y)); //## NOTE: Pr(X >= x) with X~NB
			fa[pos] = (float) p;
		    }
		    switch ( ref.charAt(++ros) ) {
		    case 'A': case 'a':
			switch ( pg.charAt(ros) ) { case 'A': case 'a': case 'N': case 'n': --y; continue;  case 'C': case 'c': case 'G': case 'g': case 'T': case 't': --x; --y; continue;  default: continue; }
		    case 'C': case 'c':
			switch ( pg.charAt(ros) ) { case 'C': case 'c': case 'N': case 'n': --y; continue;  case 'A': case 'a': case 'G': case 'g': case 'T': case 't': --x; --y; continue;  default: continue; }
		    case 'G': case 'g':
			switch ( pg.charAt(ros) ) { case 'G': case 'g': case 'N': case 'n': --y; continue;  case 'A': case 'a': case 'C': case 'c': case 'T': case 't': --x; --y; continue;  default: continue; }
		    case 'T': case 't':
			switch ( pg.charAt(ros) ) { case 'T': case 't': case 'N': case 'n': --y; continue;  case 'A': case 'a': case 'C': case 'c': case 'G': case 'g': --x; --y; continue;  default: continue; }
		    case 'N': case 'n': case 'X': case 'x':
			switch ( pg.charAt(pos) ) { case '?': continue;  default: ++y; continue; }
		    }
		}
		//## assessing each position using the harmonic mean p-value of the ws corresponding NB p-values
		//## NOTE: here, fa[pos] is the NB p-value for the region [ros, pos]
		pos = sr[r]; end = ( ++r == k ) ? lref : sr[r]; --r; --pos;
		sum = 0; l = pos + ws; 
		while ( ++pos < l ) sum += 1.0 / fa[pos];
		ros = sr[r]; --ros; --pos; 
		while ( ++pos < end ) {
		    sum += 1.0 / fa[pos];
		    hmp = ws / sum;
		    ++ros;
		    if ( hmp < pvSNP && pc.charAt(ros) != 'U' ) pc.setCharAt(ros, 'X');
		    sum -= 1.0 / fa[ros];
		}
	    }
	    fa = null;
	}

	
	//##########################################################################################################
	//### writing map file                                                                                   ###
	//##########################################################################################################
	nU = nu = nO = no = nS = nM = nX = nD = nm = nN = 0;
	out = new BufferedWriter(new FileWriter(new File(basename + ".map")));
	out.write("#\tref\tseq\tA\tC\tG\tT\tgap\trev\tmap\tvar"); out.newLine(); //## NOTE: field description
	out.write(String.valueOf(k));                             out.newLine(); //## NOTE: no. sequences
	out.write(String.valueOf(lref));                          out.newLine(); //## NOTE: ref length
	out.write(String.format(Locale.US, "%.0f", arl));         out.newLine(); //## NOTE: avg. read length
	sb = new StringBuilder("");
	i = -1;
	while ( ++i < k ) {
	    if ( (r=Arrays.binarySearch(sh, (line=fhList.get(i)))) < 0 ) continue;
	    pos = sr[r]; end = ( ++r == k ) ? lref : sr[r]; --r; 
	    out.write(">" + line + "\t" + (end-pos)); out.newLine();
	    --pos; c = 0;
	    while ( ++pos < end ) {
		mismatch = ( pg.charAt(pos) != Character.toUpperCase(ref.charAt(pos)) );
		switch ( (co=pc.charAt(pos)) ) {
		case 'U': ++nU; mismatch = false; break;
		case 'u': ++nu;                   break;
		case 'O': ++nO;                   break;
		case 'o': ++no;                   break;
		case 'S': ++nS;                   break;
		case 'X': case 'x': ++nX;         break;
		default:  ++nM;                   break;
		}
		if ( mismatch ) 
		    switch ( (ch=Character.toUpperCase(pg.charAt(pos))) ) {
		    case '-':                                                                                                     ++nD; break;
		    case 'M': case 'V': case 'N': case 'H': case 'R': case 'D': case 'W': case 'S': case 'B': case 'Y': case 'K': ++nN; break;
		    case 'A': case 'C': case 'G': case 'T':                                                                       ++nm; break;
		    }
		sb.setLength(0);
		sb = sb.append((++c)).append('\t')        //## NOTE: position
		    .append(ref.charAt(pos)).append('\t') //## NOTE: ref base
		    .append(pg.charAt(pos)).append('\t')  //## NOTE: inferred base
		    .append(oA[pos]).append('\t')         //## NOTE: no. As
		    .append(oC[pos]).append('\t')         //## NOTE: no. Cs
		    .append(oG[pos]).append('\t')         //## NOTE: no. Gs
		    .append(oT[pos]).append('\t')         //## NOTE: no. Ts
		    .append(og[pos]).append('\t')         //## NOTE: no. gaps
		    .append(or[pos]).append('\t')         //## NOTE: no. reverse strand reads
		    .append(pc.charAt(pos)).append('\t')  //## NOTE: mapping code
		    .append(((mismatch)?"1":"0"));        //## NOTE: 1 if inferred mismatch
		out.write(sb.toString());
		out.newLine();
	    }
	}
	out.close();
	if ( verb ) {
	    System.err.println("");
	    System.err.println("map codes               "
			       + "  U=" + nU
			       + "  u=" + nu
			       + "  O=" + nO
			       + "  o=" + no
			       + "  S=" + nS
			       + "  M=" + nM
			       + ((pvSNP>0)?"  X=" + nX:""));
	    System.err.println("mismatche(s)              " + nm);
	    System.err.println("degenerate(s)             " + nN);
	    System.err.println("");
	    System.err.println("output map:               " + basename + ".map");
	}
 
	
	//##########################################################################################################
	//### writing inferred pseudo-genome in FASTA format                                                     ###
	//##########################################################################################################
	if ( pvSNP > 0 ) { //## NOTE: setting 'x' at positions encoded by 'X' 
	    pos = lref; while ( --pos >= 0 ) if ( (ch=pc.charAt(pos)) == 'X' || ch == 'x' ) pg.setCharAt(pos, 'x');
	}
	out = new BufferedWriter(new FileWriter(new File(basename + ".fasta")));
	if ( pgname.equals(NOTHING) ) {
	    i = -1;
	    while ( ++i < k ) {
		if ( (r=Arrays.binarySearch(sh, (line=fhList.get(i)))) < 0 ) continue;
		out.write(">" + line + " consensus"); out.newLine();
		end = ( ++r == k ) ? lref : sr[r];
		out.write(pg.substring(sr[--r], end)); out.newLine();
	    }
	}
	else {
	    out.write(">" + pgname); out.newLine();
	    i = -1;
	    while ( ++i < k ) {
		if ( (r=Arrays.binarySearch(sh, (line=fhList.get(i)))) < 0 ) continue;
		end = ( ++r == k ) ? lref : sr[r];
		out.write(pg.substring(sr[--r], end)); 
	    }
	    out.newLine();
	}
	out.close();
	if ( verb ) {
	    System.err.println("output sequence(s):       " + basename + ".fasta");
	}
    }



    //##### quickly splits a SAM tsv line
    //##### reads only the 11 first entries
    final static String[] samSplit(final String row) {
	String[] split = new String[11];
	int x = 0, y = row.indexOf('\t'); // split[0]  = row.substring(x, y);                              //## NOTE: read id  => not used
	y = row.indexOf('\t', (x=++y));      split[1]  = row.substring(x, y);                              //## NOTE: flag
	y = row.indexOf('\t', (x=++y));      split[2]  = row.substring(x, y);                              //## NOTE: contig name
	y = row.indexOf('\t', (x=++y));      split[3]  = row.substring(x, y);                              //## NOTE: alignment position
	y = row.indexOf('\t', (x=++y));      split[4]  = row.substring(x, y);                              //## NOTE: mapQ
	y = row.indexOf('\t', (x=++y));      split[5]  = row.substring(x, y);                              //## NOTE: CIGAR
	y = row.indexOf('\t', (x=++y));   // split[6]  = row.substring(x, y);                              //                  => not used
	y = row.indexOf('\t', (x=++y));   // split[7]  = row.substring(x, y);                              //                  => not used
	y = row.indexOf('\t', (x=++y));   // split[8]  = row.substring(x, y);                              //                  => not used
	y = row.indexOf('\t', (x=++y));      split[9]  = row.substring(x, y);                              //## NOTE: read sequence
	y = row.indexOf('\t', (x=++y));      split[10] = (y < 0) ? row.substring(x) : row.substring(x, y); //## NOTE: Phred score sequence
	if ( split[10].length() != split[9].length() ) split[10] = split[9];                               //## NOTE: no Phred score 
	return split;
    }

    
    //##### estimates the w*Poisson(lambda) + (1-w)*NB(p,r) parameters from an observed distribution
    //##### a Poisson distribution is used to deal with zero-tail values
    //##### NB distribution is used to fit remaining values
    final static int MAX_ITER = 50;                     // max. iteration for distribution fitting
    final static double PTNBFIT_PMIN = 0.001;           // lower bound p-value for PtNB fitting
    final static double EPS = 1E-8;                     // convergence criterion
    final static double[] fitPtNB(final int[] distribution) {
	int l = distribution.length;
	//## estimating Poisson(lambda) tail from the first non-zero values
	double sumyP = 1, sumxyP = 1; //## NOTE: default 1 to deal with useless Poisson tail
	sumyP += distribution[0];
	double lambda = sumxyP / sumyP;                                                               //System.out.println("start P " + lambda);
	//## iteratively estimating (i) NB(p, r) and (ii) Poisson(lambda) tail
	int[] dist = new int[x=l]; double[] pr; double p = 0, r = 0, fx, sumyNB, crit = Double.MAX_VALUE;
	int iter = MAX_ITER;
	while ( --iter >= 0 ) {
	    //## (i) estimating NB(p, r) from data cleared of (Poisson) tail values
	    sumyNB = 0; x = l;                                                                        //System.out.println("" + x);
	    while ( --x >= 0 ) sumyNB += (dist[x] = (int) Math.max(0, distribution[x] - sumyP*pmfPoisson(x, lambda)));
	    pr = fitNB(dist); p = pr[0]; r = pr[1];                                                   //System.out.println(iter + " NB " + p + " " + r);
	    //## (ii) estimating Poisson(lambda) tail from x=0,1 data cleared of NB-associated values
	    sumyP = 1; sumxyP = 1; 
	    fx = (int) (distribution[0] - sumyNB*pmfNB(0, p, r));
	    fx = (fx < 0) ? 0: fx;
	    sumyP += fx;
	    if ( l > 1 && cdfNB(1, p, r) < PTNBFIT_PMIN ) {     //## NOTE: < PTNBFIT_PMIN to not intrude on NB-related data
		fx = (int) (distribution[1] - sumyNB*pmfNB(1, p, r));
		fx = (fx < 0) ? 0: fx;
		sumyP += fx;
		sumxyP += fx;
	    }
	    lambda = sumxyP / sumyP;                                                                  //System.out.println(iter + " P " + lambda);
	    //## testing convergence
	    if ( Math.abs(crit-(fx=lambda*p*(Double.isInfinite(r)?1:r))) < EPS ) break;
	    crit = fx;                                                                                //System.out.println(iter + " crit " + crit);
	}
	//## returning results
	double w = 0; x = l; while ( --x >= 0 ) w += distribution[x];
	w = sumyP / w;
	pr = new double[5];
	pr[0] = lambda; //## NOTE: lambda parameter of the Poisson tail distribution
	pr[1] = w;      //## NOTE: weight associated to Poisson
	pr[2] = p;      //## NOTE: NB p parameter
	pr[3] = r;      //## NOTE: NB r parameter
	pr[4] = 1 - w;  //## NOTE: weight associated to NB
	return pr;
    }
    
    //##### estimates the NB(p,r) parameters from an observed distribution
    //##### outlier thresholds xmin and xmax are iteratively reestimated to bound the observed values to fit
    //##### observed values lower/higher than xmin/xmax are replaced by the theoretical NB ones
    final static double DG_MIN = 1E-8;                  // dg min threshold for NB fitting
    final static double DG_MAX = 1E20;                  // dg max threshold for NB fitting
    final static double P_MIN = 1E-8;                   // p min threshold for NB fitting
    final static double NBFIT_PMIN = 0.025;             // lower bound p-value for NB fitting
    final static double NBFIT_PMAX = 0.975;             // upper bound p-value for NB fitting
    final static double[] fitNB(final int[] distribution) {
	double[] pr = new double[2];
	//## 1. estimating the (starting) parameter values p and r of the theoretical NB distribution
	int[] dist = Arrays.copyOf(distribution, distribution.length);
	int l = distribution.length;
	while ( dist[--l] == 0 ) {} ++l;
	int xmin = 0, xmax = l;
	//## NOTE: avg: observed mean
	//## NOTE: var: observed variance
	double sumy = 0, sumxy = 0, sumxxy = 0, fx, tmp, rmax;
	int x = l; while ( --x >= 0 ) { sumy += (fx=dist[x]); sumxy += (tmp=x*fx); sumxxy += x*tmp; }
	double avg = sumxy / sumy, var = (sumxxy-sumxy)/(sumy-1);
	double r = avg*avg/(var-avg), p = 1 - avg/var;                              //System.out.println("  NB" + " s" + " " + p + " " + r);
	//## 2. searching for the best values p and r by iteratively (i) optimizing ML criterion and (ii) updating xmin and xmax for discarding outliers
	double dg, up, dn; int iter = MAX_ITER, it, k, xminnew, xmaxnew;
	while ( --iter > 0 ) {
	    rmax = sumxy * (1/P_MIN - 1) / sumy;  //## NOTE: p := sumxy/(sumxy + sumy*r) should be > P_MIN
	    // (i)
	    dg = up = dn = 1; r = r + 1; it = MAX_ITER;
	    while ( (! Double.isInfinite(dg)) && (! Double.isNaN(dg)) && (Math.abs(dg) > DG_MIN) && (Math.abs(dg) < DG_MAX) && (Math.abs(r-dg) < rmax) && (--it > 0) ) { 
		r = ((r-=dg) < 0) ? -r : r;                                         //System.out.println("  NB r " + r);
		tmp = sumxy + sumy*r; up = sumy*Math.log(sumy*r/tmp); dn = sumy*sumxy/(r*tmp);
		x = l; while ( --x >= 0 ) { fx = dist[(k=x)]; tmp = r-1; while ( --k >= 0 ) { up += fx/(++tmp); dn -= fx/(tmp*tmp); } }
		dg = up/dn;
	    }
	    if ( Math.abs(r-dg) > rmax ) { //## NOTE: switching to (Generalized) Poisson
		avg = sumxy / sumy;
		var = 0; x = l; while ( --x >= 0 ) var += dist[x]*square(avg-x);
		var /= sumy;
		pr[0] = Math.sqrt(avg*avg*avg/var); pr[1] = 1 - Math.sqrt(avg/var); //System.out.println("  GP lambda theta " + pr[0] + " " + pr[1]);
		if ( pr[1] >= 0 ) {        //## NOTE: not underdispersed, switching to Poisson
		    pr[0] = avg; pr[1] = Double.POSITIVE_INFINITY;                  //System.out.println("  NB lambda " + pr[0]);
		}
		return pr;
 	    }
	    p = sumxy / (sumxy + sumy*r);                                           //System.out.println("  NB p " + p);
	    // (ii)
	    x = -1; fx = 0; while ( ++x < xmin ) fx += pmfNB(x, p, r);
	    xminnew = xmin; xmaxnew = xmax; --x; while ( (++x < xmax) && ((fx+=pmfNB(x, p, r)) <= NBFIT_PMAX) ) if ( fx < NBFIT_PMIN ) xminnew = x; xmaxnew = x;
	    ++xmaxnew; if ( (xmin == xminnew) && (xmax == xmaxnew) ) break;
	    xmin = xminnew; xmax = xmaxnew;                                        // System.out.println("  NB xmin " + xmin + "  xmax " + xmax);
	    x = l; while ( --x >= 0 ) if ( x < xmin || x > xmax ) dist[x] = (int) (sumy * pmfNB(x, p, r));
	    sumy = sumxy = 0; x = l; while ( --x >= 0 ) { sumy += (fx=dist[x]); sumxy += x*fx; }
	}
	pr[0] = p; pr[1] = r;
	return pr;
    }
	
    //##### r >= 0   : estimates the NB(p,r) PMF, i.e. P(X=x) with X~NB(p,r)
    //##### r = +inf : estimates the Poisson(p) PMF 
    //##### r < 0    : estimates the GP(p,r) PMF 
    final static double pmfNB(final int x, final double p, final double r) {
	return ( Double.isInfinite(r) ) ? pmfPoisson(x, p) : ( r < 0 ) ? pmfGP(x, p, r)
	    :  ( x == 0 )               ? Math.pow(1-p, r) : Math.exp(gammln(r+x) - gammln(x+1) - gammln(r) + x*Math.log(p) + r*Math.log(1-p));
    }

    //##### r >= 0   : estimates the NB(p,r) CDF
    //##### r = +inf : estimates the Poisson(p) CDF
    //##### r < 0    : estimates the GP(p,r) CDF 
    final static double cdfNB(final int x, final double p, final double r) {
	return ( Double.isInfinite(r) ) ? cdfPoisson(x, p) : ( r < 0 ) ? cdfGP(x, p, r) : 1 - betai(x+1, r, p);
    }

    //##### estimates the inverse NB(p,r) CDF, i.e. the largest x st. CDF(x) < pvalue
    final static int cdfNBinv(final double pvalue, final double p, final double r) {
	int x = -1; while ( cdfNB(++x, p, r) < pvalue ) {}
	return x;
    }

    //##### estimates the Poisson(lambda) PMF, i.e. P(X=x) with X~Poisson(lambda)
    final static double pmfPoisson(final int x, final double lambda) {
	return ( x == 0 ) ? Math.exp(-lambda) : Math.exp(x*Math.log(lambda) - lambda - gammln(x+1));
    }

    //##### estimates the Poisson(lambda) CDF
    final static double cdfPoisson(final int x, final double lambda) {
	return gammq(x+1, lambda);
    }

    //##### estimates the GP(lambda, theta) PMF, i.e. P(X=x) with X~GP(lambda, theta)
    //##### GP = Generalized Poisson, e.g. Consul (1989) Generalized Poisson Distributions: Properties and Applications. Marcel Dekker Inc., New York/Basel
    //##### NOTE: here, theta < 0 to obtain a underdispersed counting distribution
    final static double pmfGP(final int x, final double lambda, final double theta) {
	return ( x == 0 ) ? Math.exp(-lambda) : Math.exp(Math.log(lambda) + (x-1)*Math.log(lambda + theta*x) - gammln(x+1) - lambda - theta*x);
    }

    //##### estimates the GP(lambda, theta) CDF
    //##### GP = Generalized Poisson, e.g. Consul (1989) Generalized Poisson Distributions: Properties and Applications. Marcel Dekker Inc., New York/Basel
    //##### NOTE: here, theta < 0 to obtain a underdispersed counting distribution
    final static double cdfGP(final int x, final double lambda, final double theta) {
	double cdf = 0; int i = -1; while ( ++i <= x ) cdf += pmfGP(i, lambda, theta);
	return cdf;
    }
    
    //##### squares the specified double value
    final static double square(final double x) {
	return x*x;
    }

    //##### estimates the regularized incomplete beta function Ix(a,b) = Bx(a,b)/B(a,b)
    final static double betai(final double a, final double b, final double x) {
	double bt = ( x == 0 || x == 1 ) ? 0 : Math.exp(gammln(a+b) - gammln(a) - gammln(b) + a*Math.log(x) + b*Math.log(1-x));
	return ( x < (a+1)/(a+b+2) ) ? bt * betacf(a, b, x) / a : 1 - bt * betacf(b, a, 1-x) / b;
    }
    final static double FPMIN = 1E-10;
    final static double betacf(final double a, final double b, final double x) {
	double qab = a + b, qap = a + 1, qam = a - 1, c = 1, d = 1 - qab * x / qap;
	d = ( Math.abs(d) < FPMIN ) ? 1/FPMIN : 1/d;
	double tmp, aa, del, h = d;
	int m = 0, m2;
	while ( ++m < MAX_ITER ) {
	    m2 = 2*m;
	    aa = m * (b-m) * x / ((qam+m2) * (a+m2));
	    d = ( Math.abs((tmp=1+aa*d)) < FPMIN ) ? FPMIN : tmp;
	    c = ( Math.abs((tmp=1+aa/c)) < FPMIN ) ? FPMIN : tmp;
	    d = 1 / d;
	    h *= d * c;
	    aa = -(a+m) * (qab+m) *x / ((a+m2) * (qap+m2));
	    d = ( Math.abs((tmp=1+aa*d)) < FPMIN ) ? FPMIN : tmp;
	    c = ( Math.abs((tmp=1+aa/c)) < FPMIN ) ? FPMIN : tmp;
	    d = 1 / d;
	    del = d * c;
	    h *= del;
	    if ( Math.abs(del-1) < EPS ) break;
	}
	return h;
    }

    //##### estimates the upper incomplete gamma function Q(a,x)
    final static double gammq(final double a, final double x) {
	if ( x < a + 1 ) {
	    double ap = a, del = 1.0 / a, sum = del;
	    int n = 0;
	    do {
		++ap;
		del *= x / ap;
		sum += del;
	    } while ( ++n < MAX_ITER && Math.abs(del) > Math.abs(sum) * EPS );
	    return 1 - sum * Math.exp( -x + a*Math.log(x) - gammln(a) );
	}
	double b = x + 1 -a, c = 1 / FPMIN, d = 1.0 / b, h = d;
	double an, del, tmp;
	int i = 0;
	while ( ++i < MAX_ITER ) {
	    an = -i*(i-a);
	    ++b; ++b;
	    d = ( Math.abs((tmp=an*d+b)) < FPMIN ) ? FPMIN : tmp;
	    c = ( Math.abs((tmp=b+an/c)) < FPMIN ) ? FPMIN : tmp;
	    d = 1 / d;
	    del = d * c;
	    h *= del;
	    if ( Math.abs(del-1) < EPS) break;
	}
	return h * Math.exp( -x + a*Math.log(x) - gammln(a) );
    }

    //##### estimates ln Gamma(x)
    final static double gammln(final double x) {
	return ( x < 0.5 ) ? 1.1447298858494 - Math.log(Math.sin(Math.PI*x)) - lanczosG5N6(1.0-x) : lanczosG5N6(x);
    }
    final static double[] LANCZOS_G5N6 = {76.18009172947146, -86.50532032941677, 24.01409824083091, -1.231739572450155, 0.001208650973866179, -0.000005395239384953};
    final static double lanczosG5N6(final double x) {
	double y = x, tmp = x + 5.5, ser = 1.000000000190015;
	tmp -= (x + 0.5) * Math.log(tmp);
	for (final double lcz: LANCZOS_G5N6) ser += lcz / (++y);
	return -tmp + Math.log(2.5066282746310005*ser/x);
    }    



    //##### compute a SNP distribution against the ref using a sliding window
    static int[] getSNPd(final int ws, final double limit, final String ref, final StringBuilder pg, final int[] sr) {
	int wmin = (int) (limit * ws); //## NOTE: ws limit, i.e. not considered if less than wmin known character states
	int[] mmd = new int[ws+1];
	int pos, ros, end, l, x, y, k = sr.length, r = -1;
	while ( ++r < k ) {
	    pos = sr[r]; end = ( ++r == k ) ? pg.length() : sr[r]; --r; --pos;
	    if ( end - pos < ws ) continue;
	    l = pos + ws;
	    x = y = 0;
	    //## NOTE: first ws-1 positions
	    while ( ++pos < l ) {
		switch ( ref.charAt(pos) ) {
		case 'A': case 'a':
		    switch ( pg.charAt(pos) ) { case 'A': case 'a': case 'N': case 'n': ++y; continue;  case 'C': case 'c': case 'G': case 'g': case 'T': case 't': ++x; ++y; continue;  default: continue; } 
		case 'C': case 'c':
		    switch ( pg.charAt(pos) ) { case 'C': case 'c': case 'N': case 'n': ++y; continue;  case 'A': case 'a': case 'G': case 'g': case 'T': case 't': ++x; ++y; continue;  default: continue; }
		case 'G': case 'g':
		    switch ( pg.charAt(pos) ) { case 'G': case 'g': case 'N': case 'n': ++y; continue;  case 'A': case 'a': case 'C': case 'c': case 'T': case 't': ++x; ++y; continue;  default: continue; }
		case 'T': case 't':
		    switch ( pg.charAt(pos) ) { case 'T': case 't': case 'N': case 'n': ++y; continue;  case 'A': case 'a': case 'C': case 'c': case 'G': case 'g': ++x; ++y; continue;  default: continue; }
		case 'N': case 'n': case 'X': case 'x':
		    switch ( pg.charAt(pos) ) { case '?': continue;  default: ++y; continue; }
		}
	    }
	    //## NOTE: up to end
	    ros = sr[r]; --ros; --pos; 
	    while ( ++pos < end ) {
		switch ( ref.charAt(pos) ) {
		case 'A': case 'a':
		    switch ( pg.charAt(pos) ) { case 'A': case 'a': case 'N': case 'n': ++y; break;  case 'C': case 'c': case 'G': case 'g': case 'T': case 't': ++x; ++y; break;  default: break; } break;
		case 'C': case 'c':
		    switch ( pg.charAt(pos) ) { case 'C': case 'c': case 'N': case 'n': ++y; break;  case 'A': case 'a': case 'G': case 'g': case 'T': case 't': ++x; ++y; break;  default: break; } break;
		case 'G': case 'g':
		    switch ( pg.charAt(pos) ) { case 'G': case 'g': case 'N': case 'n': ++y; break;  case 'A': case 'a': case 'C': case 'c': case 'T': case 't': ++x; ++y; break;  default: break; } break;
		case 'T': case 't':
		    switch ( pg.charAt(pos) ) { case 'T': case 't': case 'N': case 'n': ++y; break;  case 'A': case 'a': case 'C': case 'c': case 'G': case 'g': ++x; ++y; break;  default: break; } break;
		case 'N': case 'n': case 'X': case 'x':
		    switch ( pg.charAt(pos) ) { case '?': break;  default: ++y; break; } break;
		}
		if ( y >= wmin ) ++mmd[x]; //## NOTE: here x/y is the p-distance within the sliding window
		switch ( ref.charAt(++ros) ) {
		case 'A': case 'a':
		    switch ( pg.charAt(ros) ) { case 'A': case 'a': case 'N': case 'n': --y; continue;  case 'C': case 'c': case 'G': case 'g': case 'T': case 't': --x; --y; continue;  default: continue; }
		case 'C': case 'c':
		    switch ( pg.charAt(ros) ) { case 'C': case 'c': case 'N': case 'n': --y; continue;  case 'A': case 'a': case 'G': case 'g': case 'T': case 't': --x; --y; continue;  default: continue; }
		case 'G': case 'g':
		    switch ( pg.charAt(ros) ) { case 'G': case 'g': case 'N': case 'n': --y; continue;  case 'A': case 'a': case 'C': case 'c': case 'T': case 't': --x; --y; continue;  default: continue; }
		case 'T': case 't':
		    switch ( pg.charAt(ros) ) { case 'T': case 't': case 'N': case 'n': --y; continue;  case 'A': case 'a': case 'C': case 'c': case 'G': case 'g': --x; --y; continue;  default: continue; }
		case 'N': case 'n': case 'X': case 'x':
		    switch ( pg.charAt(pos) ) { case '?': continue;  default: ++y; continue; }
		}
	    }
	}
	return mmd;
    }
    


    //##### writes cov.txt file
    static void writeCov(final File outfile, final int[] distrib, final double[] cnbzp, final int lref, final int min, final int max, final int depth) throws IOException {
	BufferedWriter out = new BufferedWriter(new FileWriter(outfile));
	out.write("=  observed coverage distribution:  no.pos=" + lref); out.newLine();
	double l = cnbzp[0], wp = cnbzp[1], p = cnbzp[2], r = cnbzp[3], wnb = cnbzp[4];
	out.write(String.format(Locale.US, "#  Poisson(l) coverage tail distribution:  l=%.8f  w=%.8f", l, wp)); out.newLine();
	if ( Double.isInfinite(r) ) out.write(String.format(Locale.US, "*  Poisson(l') coverage distribution:  l'=%.8f  1-w=%.8f", p, wnb));
	else if ( r < 0 )           out.write(String.format(Locale.US, "*  GP(l',r) coverage distribution:  l'=%.8f  r=%.8f  1-w=%.8f", p, r, wnb));
	else 	                    out.write(String.format(Locale.US, "*  NB(p,r) coverage distribution:  p=%.8f  r=%.8f  1-w=%.8f", p, r, wnb));
	out.newLine();
	int sumy = 0;
	for (int y: distrib) sumy += y;
	double tmp, up = 0; int x = distrib.length;
	while ( --x > 2 ) { if ( up < (tmp=distrib[x]/(double)sumy) ) up = tmp; if ( up < (tmp=wnb*pmfNB(x, p, r)) ) up = tmp; }
	out.write("         0" + BLANK.substring(0, depth-1) + ((int)(up*sumy))); out.newLine();
	out.write("cov      |" + BLANK.substring(0, depth-1).replaceAll(" ", "-") + "|"); out.newLine();
	double scale = depth / up;
	int u, v, z, y;
	x = -1;
	while ( ++x < Math.min(max+20, distrib.length) ) {
	    out.write((x + BLANK).substring(0,5) + ((x == min) ? "min-" : (x == max) ? "max-" : "    "));
	    u = (int) (wnb * pmfNB(x, p, r) * scale);
	    v = (int) (wp * pmfPoisson(x, l) * scale);
	    z = (int) (distrib[x] * scale / sumy);
	    y = -1; while ( ++y <= depth ) out.write((y == u)?"*":(y == v)?"#":(y<=z)?"=":" ");
	    out.write(" " + distrib[x]);
	    out.newLine();
	}
	z = 0; u=(--x); while ( ++x < distrib.length ) z += distrib[x];
	out.write((">" + u + BLANK).substring(0,5) + "    ");
	y = -1; while ( ++y <= z*scale/sumy ) out.write("=");
	y -= 2; while ( ++y <= depth ) out.write(" "); out.write("" + z); out.newLine();
	out.close();
    }



    //##### writes snp.txt file
    static void writeSnp(final File outfile, final int[] distrib, final double[] snb, final int ws, final int min, final int max, final int depth) throws IOException {
	BufferedWriter out = new BufferedWriter(new FileWriter(outfile));
	out = new BufferedWriter(new FileWriter(new File(basename + ".snp.txt")));
	out.write("=  observed SNP distribution:  window size = " + ws); out.newLine();
	double p = snb[0], r = snb[1];
	out.write(String.format(Locale.US, "*  NB(p,r) theoretical distribution:  p=%.8f  r=%.8f", p, r)); out.newLine();
	int sumy = 0;
	for (int y: distrib) sumy += y;
	double tmp, up = 0; int x = distrib.length;
	while ( --x >= 0 ) { if ( up < (tmp=distrib[x]/(double)sumy) ) up = tmp; if ( up < (tmp=pmfNB(x, p, r)) ) up = tmp; }
	out.write("           0" + BLANK.substring(0, depth-1) + ((int)(up*sumy))); out.newLine();
	out.write("snp        |" + BLANK.substring(0, depth-1).replaceAll(" ", "-") + "|"); out.newLine();
	double scale = depth / up;
	int u, z, y;
	x = -1;
	while ( ++x < Math.min(max+20, ws) ) {
	    out.write((x + BLANK).substring(0,5) + ((x == min) ? " 0.5%-" : (x == max) ? "99.5%-" : "      ")); //## NOTE: min and max correspond to 99% CI in the main
	    u = (int) (pmfNB(x, p, r) * scale);
	    z = (int) (distrib[x] * scale / sumy);
	    y = -1; while ( ++y <= depth ) out.write((y == u)?"*":(y<=z)?"=":" ");
	    out.write(" " + distrib[x]);
	    out.newLine();
	}
	z = 0; u=(--x); while ( ++x < distrib.length ) z += distrib[x];
	out.write((">" + u + BLANK).substring(0,5) + "      ");
	y = -1; while ( ++y <= z*scale/sumy ) out.write("=");
	y -= 2; while ( ++y <= depth ) out.write(" "); out.write("" + z); out.newLine();
	out.close();
    }
    
}

