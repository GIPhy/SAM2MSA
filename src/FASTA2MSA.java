/*
  ########################################################################################################

  FASTA2MSA: combining consensus sequences into a multiple sequence alignment
  
  Copyright (C) 2020-2023  Institut Pasteur

  This program is part of the package SAM2MSA.
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr
   Centre de Ressources Biologiques de l'Institut Pasteur (CRBIP)             research.pasteur.fr/en/b/VTq
   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr

  ########################################################################################################
*/

import java.io.*;
import java.util.*;
import java.util.regex.*;

public class FASTA2MSA {

    //### constants  ################################################################
    final static String VERSION = "0.1.200711c";
    final static String NOTHING = "N.o./.T.h.I.n.G";
    final static String STDIN = "-";
    final static String BLANK = "                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ";
    final static char DELCHAR = '@';
    final static Pattern CANCELER = Pattern.compile(String.valueOf(DELCHAR) + "+");
    final static char NOC1 = '1';
    final static char NOC2 = '2';
    final static byte B1 = (byte) 1;
    final static byte B2 = (byte) 2;
    final static byte B3 = (byte) 3;
    final static byte B_1 = (byte) -1;
    final static byte B_2 = (byte) -2;
    final static byte B_3 = (byte) -3;

    //### io  #######################################################################
    static BufferedReader in;
    static BufferedWriter out;

    //### options  ##################################################################
    static File dataFile;         // infile (mandatory option -i)
    static String basename;       // output basename for output files (mandatory option -o)
    static File gffFile;          // gff3 file (option -g; default; none)
    static char[] unknown;        // unknown char. states (option -u; default: xXnN-?)
    static double ufreq;          // max. proportion of allowed unknown char. states (option -p; default: 0.5)
    static char[] forbidden;      // forbidden char. states (option -f; default: none)
    static boolean onlyVar;       // only variable characters (option -V; default: not set)
    static boolean cds1;          // only codon positions 1 (option -1; requires option -g; default: not set)
    static boolean cds2;          // only codon positions 2 (option -2; requires option -g; default: not set)
    static boolean cds3;          // only codon positions 3 (option -3; requires option -g; default: not set)
    static int gcode;             // genetic code id (default: 1)
    static int rstart;            // starting index of the region to select (option -s; default 0)
    static int rend;              // ending index of the region to select (option -e; default 0)
    static boolean verb;          // verbose mode (option -v)

    //### data  #####################################################################
    static ArrayList<String> fh;     // sequence names
    static ArrayList<File> infiles;  // list of FASTA files
    static int n;                    // no. FASTA files
    static int lref;                 // MSA length
    static String[] msa;             // MSA
    static BitSet var;               // variable characters
    static BitSet keep;              // characters to be kept
    static boolean unkExists;        // true when unknown is not empty
    static boolean delForbidden;     // true when forbidden is not empty
    static int[] fpos;               // indices of output MSA
    static ArrayList<String> lbl;    // GFF3: sequence names
    static ArrayList<Integer> sr;    // GFF3: starting indexes
    static ArrayList<String> lblCDS; // GFF3: CDS labels
    static int[] idCDS1;             // GFF3: CDS ids
    static byte[] cod1;              // GFF3: CDS coding position
    static int[] idCDS2;             // GFF3: CDS ids
    static byte[] cod2;              // GFF3: CDS coding position
    
    //### stuffs  ###################################################################
    static int c, i, k, l, o, r, x, y;
    static int max, pos, end, unk;
    static double ucutoff;
    static char cs, c1, c2;
    static byte cod, bc1, bc2;
    static boolean p1, p2, p3;
    static String line, codon;
    static StringBuilder sb;
    static File f;
    static String[] split;
    
    public static void main(String[] args) throws IOException {

	//##########################################################################################################
	//### man                                                                                                ###
	//##########################################################################################################
	if ( args.length < 2 ) {
	    System.out.println("");
	    System.out.println(" FASTA2MSA v." + VERSION + "  Copyright (C) 2020  Institut Pasteur");
	    System.out.println("");
	    System.out.println(" FASTA2MSA combines  the different  consensus sequences  estimated by  SAM2MAP or");
	    System.out.println(" MAP2FASTA into a multiple sequence alignment (MSA) in FASTA format.");
	    System.out.println("");
	    System.out.println(" USAGE: FASTA2MSA [-i INFILE] [-o BASENAME] ...");
	    System.out.println("");
	    System.out.println(" GENERAL OPTIONS:");
	    System.out.println("");
	    System.out.println("   -i FILE      text file containing one line per taxon, each line containing the");
	    System.out.println("                taxon  name  followed  by  the  corresponding  FASTA  file  name,");
	    System.out.println("                separated by blank space(s) or tabulation(s) (mandatory)");
	    System.out.println("   -o BASENAME  basename for output files (mandatory)");
	    System.out.println("   -v           verbose mode");
	    System.out.println("");
	    System.out.println(" CHARACTER FILTERING:");
	    System.out.println("");
	    System.out.println("   -u STRING    the character state(s) to be considered as unknown ones (default:");
	    System.out.println("                \"xXnN-?\")");
	    System.out.println("   -p NUMBER    the maximum  allowed frequency  of unknown  character states  per");
	    System.out.println("                aligned character (default: 0.5)");
	    System.out.println("   -f STRING    the  character  state(s)  to be  considered  as  forbidden;  each");
	    System.out.println("                character containing at least one  forbidden character state will");
	    System.out.println("                be discarded (default: \"\")");
	    System.out.println("   -V           when set, only variable characters will be selected (default: not");
	    System.out.println("                set)");
	    System.out.println("   -s NUMBER    discarding all characters before the specified position (default:");
	    System.out.println("                not set)");
	    System.out.println("   -e NUMBER    discarding all characters  after the specified position (default:");
	    System.out.println("                not set)");
	    System.out.println("");
	    System.out.println(" CODING REGIONS:");
	    System.out.println("");
	    System.out.println("   -g FILE      GFF3 file containing  annotations of the  reference sequence used");
	    System.out.println("                for creating  the input  FASTA files  (same contig  order as  the");
	    System.out.println("                FASTA reference sequence file)");
	    System.out.println("   -1           only codon positions 1 will be selected  (requires option -g; can");
	    System.out.println("                be combined with options -2 and -3)");
	    System.out.println("   -2           only codon positions 2 will be selected  (requires option -g; can");
	    System.out.println("                be combined with options -1 and -3)");
	    System.out.println("   -3           only codon positions 3 will be selected  (requires option -g; can");
	    System.out.println("                be combined with options -1 and -2)");
	    System.out.println("   -G NUMBER    genetic code  identifier according  to the translation  tables at");
	    System.out.println("                www.ncbi.nlm.nih.gov/Taxonomy/Utils/wprintgc.cgi (default: 1)");
	    System.out.println("");

	    System.exit(0);
	}

	
	//##########################################################################################################
	//### reading options                                                                                    ###
	//##########################################################################################################
	dataFile = new File(NOTHING);      // option -i
	gffFile = new File(NOTHING);       // option -g
	basename = NOTHING;                // option -o
	unknown = "xXnN-?".toCharArray();  // option -u
	ufreq = 0.5;                       // option -p
	forbidden = new char[0];           // option -f
	onlyVar = false;                   // option -V
	cds1 = false;                      // option -1
	cds2 = false;                      // option -2
	cds3 = false;                      // option -3
	gcode = 1;                         // option -G
	rstart = 0;                        // option -s
	rend = 0;                          // option -e
	verb = false;                      // option -v
	o = -1;
	while ( ++o < args.length ) {
	    if ( args[o].equals("-i") )     { dataFile = new File(args[++o]);        continue; }
	    if ( args[o].equals("-g") )     { gffFile = new File(args[++o]);         continue; }
	    if ( args[o].equals("-o") )     { basename = args[++o];                  continue; }
	    if ( args[o].equals("-u") )     { unknown = args[++o].toCharArray();     continue; }
	    if ( args[o].equals("-p") ) try { ufreq = Double.parseDouble(args[++o]); continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -p)"); System.exit(1); }
	    if ( args[o].equals("-f") )     { forbidden = args[++o].toCharArray();   continue; }
	    if ( args[o].equals("-V") )     { onlyVar = true;                        continue; }
	    if ( args[o].equals("-1") )     { cds1 = true;                           continue; }
	    if ( args[o].equals("-2") )     { cds2 = true;                           continue; }
	    if ( args[o].equals("-3") )     { cds3 = true;                           continue; }
	    if ( args[o].equals("-G") ) try { gcode = Integer.parseInt(args[++o]);   continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -G)"); System.exit(1); }
	    if ( args[o].equals("-s") ) try { rstart = Integer.parseInt(args[++o]);  continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -s)"); System.exit(1); }
	    if ( args[o].equals("-e") ) try { rend = Integer.parseInt(args[++o]);    continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -e)"); System.exit(1); }
	    if ( args[o].equals("-v") )     { verb = true;                           continue; }
	}
 	//### testing mandatory options -i and -b
	if ( dataFile.toString().equals(NOTHING) )                                     { System.err.println("input file not specified (option -i)");                   System.exit(1); }
	if ( ! dataFile.exists() && ! dataFile.toString().equals(STDIN) )              { System.err.println("input file does not exist (option -i)");                  System.exit(1); }
	if ( basename.equals(NOTHING) )                                                { System.err.println("output file basename not specified (option -b)");         System.exit(1); }
 	//### testing option -g
	if ( ! gffFile.toString().equals(NOTHING) && ! gffFile.exists() )              { System.err.println("gff3 file does not exist (option -g)");                   System.exit(1); }
	//### testing numerical options
	if ( ufreq < 0 || ufreq > 1 )                                                  { System.err.println("incorrect proportion value: " + ufreq + " (option -p)");  System.exit(1); }
	if ( rstart <= 0 ) rstart = 1;
	--rstart;
	if ( rend > 0 && rend < rstart )                                               { System.err.println("ending index too small: " + rend + " (option -e)");       System.exit(1); }
	if ( gcode < 1 || gcode > CODON_LIST.length || CODON_LIST[gcode].length == 0 ) { System.err.println("incorrect genetic code: " + gcode + " (option -G)");      System.exit(1); }
	//### special character states
	Arrays.sort(unknown);
	unkExists = ( unknown.length != 0 );
	Arrays.sort(forbidden);
	delForbidden = ( forbidden.length != 0 );


	//##########################################################################################################
	//### reading dataFile                                                                                   ###
	//##########################################################################################################
	if ( verb ) {
	    System.err.println("Reading " + dataFile.toString() + " ...");
	}
	fh = new ArrayList<String>();
	infiles = new ArrayList<File>();
	in = ( dataFile.toString().equals(STDIN) ) ? new BufferedReader(new InputStreamReader(System.in)) : new BufferedReader(new FileReader(dataFile));
	max = 0;
	n = 0;
	while ( true ) {
	    try { line = in.readLine().trim(); } catch ( NullPointerException e ) { in.close(); break; }
	    if ( line.length() == 0 || line.charAt(0) == '%' || line.charAt(0) == '#' ) continue;
	    split = line.split("\\s+");
	    ++n;
	    if ( split.length < 2 ) { System.err.println("incorrect number of entries at line " + n + ": " + line); System.exit(1); }
	    fh.add((line=split[0]));
	    max = ( max < (x=line.length()) ) ? x : max;
	    f = new File(split[1]);
	    if ( ! f.exists() ) { System.err.println("FASTA file does not exist for line " + n + " (" + line + "): " + f.toString()); System.exit(1); }
	    infiles.add(f);
	}
	n = fh.size();
	if ( verb ) {
	    i = -1; while ( ++i < n ) System.err.println("  " + (fh.get(i)+BLANK).substring(0,max) + "  " + infiles.get(i).toString());
	    System.err.println("no. FASTA files           " + n);
	    System.err.println("");
	}
	
	    
	//##########################################################################################################
	//### reading FASTA files and storing sequences                                                          ###
	//##########################################################################################################
	if ( verb ) {
	    System.err.println("Reading FASTA files ...");
	}
	msa = new String[n];
	lref = -1;
	i = -1;
	while ( ++i < n ) {
	    f = infiles.get(i);
	    sb = new StringBuilder("");
	    in = new BufferedReader(new FileReader(f));
	    while ( true ) {
		try { line = in.readLine().trim(); } catch ( NullPointerException e ) { in.close(); break; }
		if ( line.length() == 0 || line.charAt(0) == '>' ) continue;
		sb = sb.append(line);
	    }
	    msa[i] = sb.toString();
	    x = sb.length();
	    if ( i == 0 ) lref = x;
	    if ( verb ) {
		System.err.println("  " + (fh.get(i)+BLANK).substring(0,max) + "  " + x + " bps");
	    }
	    if ( x != lref ) { System.err.println("incompatible sequence length in FASTA file " + f.toString()); System.exit(1); }
	}
	if ( verb ) {
	    System.err.println("no. aligned characters    " + lref);
	    System.err.println("");
	}
	rend = ( rend <= 0 ) ? lref : rend;
	
	    
	//##########################################################################################################
	//### reading GFF3 file                                                                                  ###
	//##########################################################################################################
	cod1 = new byte[lref];            // CDS coding position
	cod2 = new byte[lref];            // CDS coding position
	if ( ! gffFile.toString().equals(NOTHING) ) {
	    //## first parsing to get sequence length(s)
	    lbl = new ArrayList<String>();  // sequence names
	    sr = new ArrayList<Integer>();  // start indexes of ref. sequences
	    in = new BufferedReader(new FileReader(gffFile));
	    while ( true ) {
		try { line = in.readLine().trim(); }
		catch ( NullPointerException e ) { System.err.println("GFF file does not start with tag \"##gff-version 3\": " + gffFile.toString() + " (option -g)"); System.exit(1); }
		if ( line.startsWith("##gff-version 3") || line.startsWith("## gff-version 3") ) break;
	    }
	    l = 0;
	    sr.add(l);
	    while ( true ) {
		try { line = in.readLine().trim(); } catch ( NullPointerException e ) { in.close(); break; }
		if ( line.startsWith("##sequence-region") ) {
		    split = line.split("\\s+");
		    if ( split.length != 4 )          { System.err.println("bad entry in GFF3 file: " + Arrays.toString(split) + " (option -g)"); System.exit(1); }
		    lbl.add(split[1]);
		    try { x = Integer.parseInt(split[3]); }
		    catch ( NumberFormatException e ) { System.err.println("bad entry in GFF3 file: " + line + " (option -g)"); System.exit(1); }
		    sr.add((l+=x));
		}
	    }
	    if ( l != lref ) {
		System.err.println("GFF file does not correspond to the number of aligned characters: " + lref + " != " + l + " (option -g)");
		System.exit(1);
	    }
	    //## second parsing to get CDS info
	    lblCDS = new ArrayList<String>(); // CDS labels
	    idCDS1 = new int[lref];           // CDS ids
	    idCDS2 = new int[lref];           // CDS ids
	    Arrays.fill(idCDS1, -1);
	    Arrays.fill(idCDS2, -1);
	    i = -1;
	    in = new BufferedReader(new FileReader(gffFile));
	    while ( true ) {
		try { line = in.readLine().trim(); } catch ( NullPointerException e ) { in.close(); break; }
		if ( line.startsWith("##") || line.startsWith("#!") ) continue;
		split = gff3Split(line);
		if ( ! split[2].toUpperCase().equals("CDS") || (r=lbl.indexOf(split[0])) < 0 ) continue;
		++i;                                         //## NOTE: CDS id
		lblCDS.add(split[8]);                        //## NOTE: CDS label
		pos = end = sr.get(r).intValue();
		try { x = Integer.parseInt(split[3]); }      //## NOTE: CDS start index
		catch ( NumberFormatException e ) { System.err.println("bad entry in GFF3 file: " + line + " (option -g)"); System.exit(1); }
		try { y = Integer.parseInt(split[4]); }      //## NOTE: CDS end index
		catch ( NumberFormatException e ) { System.err.println("bad entry in GFF3 file: " + line + " (option -g)"); System.exit(1); }
		try { cod = Byte.parseByte(split[7]); }      //## NOTE: phase
		catch ( NumberFormatException e ) { System.err.println("bad entry in GFF3 file: " + line + " (option -g)"); System.exit(1); }
		if ( split[6].length() == 0 )     { System.err.println("bad entry in GFF3 file: " + line + " (option -g)"); System.exit(1); }
		if ( split[6].charAt(0) == '+' ) {           //## NOTE: fwd strand
		    pos += x; --pos; --pos; end += y;
		    while ( ++pos < end && idCDS1[pos] < 0 ) {}
		    if ( pos == end ) {                      //## NOTE: CDS array 1
			pos = sr.get(r).intValue(); pos += x; --pos; --pos;
			while ( ++pos < end ) {
			    idCDS1[pos] = i;
			    cod1[pos] = (++cod);
			    cod = ( cod == B3 ) ? 0 : cod;
			}
		    }
		    else {                                   //## NOTE: CDS array 2 (to avoid CDS overlap)
			pos = sr.get(r).intValue(); pos += x; --pos; --pos;
			while ( ++pos < end ) {
			    idCDS2[pos] = i;
			    cod2[pos] = (++cod);
			    cod = ( cod == B3 ) ? 0 : cod;
			}
		    }
		} 
		if ( split[6].charAt(0) == '-' ) {           //## NOTE: rev strand
		    cod = (byte) -cod;
		    pos += y; end += x; --end; --end;
		    while ( --pos > end && idCDS1[pos] < 0 ) {}
		    if ( pos == end ) {                      //## NOTE: CDS array 1
			pos = sr.get(r).intValue(); pos += y; 
			while ( --pos > end ) {
			    idCDS1[pos] = i;
			    cod1[pos] = (--cod);
			    cod = ( cod == B_3 ) ? 0 : cod;
			}
		    }
		    else {                                   //## NOTE: CDS array 2 (to avoid CDS overlap)
			pos = sr.get(r).intValue(); pos += y; 
			while ( --pos > end ) {
			    idCDS2[pos] = i;
			    cod2[pos] = (--cod);
			    cod = ( cod == B_3 ) ? 0 : cod;
			}
		    }
		}	
	    }
	}
	
	
	//##########################################################################################################
	//### getting variable characters and setting characters to discard                                      ###
	//##########################################################################################################
	var = new BitSet(lref);
	keep = new BitSet(lref);
	keep.set(0, lref);
	ucutoff = n * ufreq;
	if ( verb ) {
	    System.err.println("Discarding characters ...");
	}
	c = lref;
	while ( --c >= 0 ) {
	    if ( c < rstart || c >= rend ) {
		keep.clear(c);
		continue;
	    }
	    if ( cds1 || cds2 || cds3 ) {
		bc1 = cod1[c];
		bc2 = cod2[c];
		p1 = (bc1 == B1 || bc1 == B_1 || bc2 == B1 || bc2 == B_1);       //## NOTE: c = codon position 1
		p2 = (bc1 == B2 || bc1 == B_2 || bc2 == B2 || bc2 == B_2);       //## NOTE: c = codon position 2
		p3 = (bc1 == B3 || bc1 == B_3 || bc2 == B3 || bc2 == B_3);       //## NOTE: c = codon position 3
		if ( ! ((cds1 && p1) || (cds2 && p2) || (cds3 && p3)) ) {
		    keep.clear(c);
		    continue;
		}
	    }		    
	    unk = 0;
	    c1 = NOC1;
	    c2 = NOC2;
	    i = n;
	    while ( --i >= 0 ) {
		cs = msa[i].charAt(c);
		if ( delForbidden && Arrays.binarySearch(forbidden, cs) >= 0 ) { //## NOTE: forbidden character state
		    unk = 2*n;
		    break;
		}
		if ( unkExists && Arrays.binarySearch(unknown, cs) >= 0 ) {      //## NOTE: unknown character state
		    ++unk;
		    continue;
		}
		if ( c1 == NOC1 ) {
		    c1 = cs;                                                     //## NOTE: first known character state
		    continue;
		}
		c2 = ( cs != c1 ) ? cs : c2;
	    }
	    if ( unk > ucutoff ) {
		keep.clear(c);
		continue;
	    }
	    if ( c2 != NOC2 ) {
		var.set(c);
		continue;
	    }
	    if ( onlyVar ) keep.clear(c);
	}
	
	if ( verb ) {
	    System.err.println("no. aligned characters    " + keep.cardinality());
	    System.err.println("no. variable characters   " + var.cardinality());
	    System.err.println("");
	}
	
	
	//##########################################################################################################
	//### writing variable character table                                                                   ###
	//##########################################################################################################
	fpos = new int[lref];
	pos = 0; c = -1; while ( (c=keep.nextSetBit(++c)) >= 0 ) fpos[c] = ++pos;
	out = new BufferedWriter(new FileWriter(new File(basename + ".var.tsv")));
	if ( gffFile.toString().equals(NOTHING) ) {
	    out.write("#pos");
	    i = -1; while ( ++i < n ) out.write("\t" + fh.get(i));
	    out.newLine();
	    c = -1;
	    while ( (c=var.nextSetBit(++c)) >= 0 ) {
		if ( ! keep.get(c) ) continue;
		out.write(String.valueOf(fpos[c]));
		i = -1; while ( ++i < n ) out.write("\t" + msa[i].charAt(c));
		out.newLine();
	    }
	}
	else {
	    out.write("#pos");
	    i = -1; while ( ++i < n ) out.write("\t" + fh.get(i));
	    out.write("\tphase");
	    i = -1; while ( ++i < n ) out.write("\t" + fh.get(i));
	    out.write("\tnonsynonymous\tattributes");
	    out.newLine();
	    c = -1;
	    while ( (c=var.nextSetBit(++c)) >= 0 ) {
		if ( ! keep.get(c) ) continue;
		out.write(String.valueOf(fpos[c]));
		i = -1; while ( ++i < n ) out.write("\t" + msa[i].charAt(c));
		if ( cod1[c] == 0 && cod2[c] == 0 ) {
		    out.write("\t0");
		    i = -1; while ( ++i < n ) out.write("\t");
		    out.write("\t");
		}
		else {
		    out.write("\t" + ((cod1[c] != 0 && cod2[c] == 0) ? cod1[c] : (cod1[c] == 0 && cod2[c] != 0) ? cod2[c] : cod1[c]+"/"+cod2[c]));
		    c1 = NOC1;
		    c2 = NOC2;
		    i = -1;
		    while ( ++i < n ) {
			codon = extractCodon(c, msa[i], cod1, cod2);
			if ( codon.length() == 3 ) {
			    cs = c2a(codon.toUpperCase(), gcode);
			    out.write("\t" + cs + " " + codon);
			    if ( cs == '?' || cs == '-' || cs == 'X' ) continue;
			    if ( c1 == NOC1 ) { c1 = cs;               continue; }
			    c2 = ( cs != c1 ) ? cs : c2;
			}
			else {
			    cs = c2a(codon.substring(0,3).toUpperCase(), gcode);
			    out.write("\t" + cs + "/" + c2a(codon.substring(4).toUpperCase(), gcode) + " " + codon);
			    if ( cs == '?' || cs == '-' || cs == 'X' ) continue;
			    if ( c1 == NOC1 ) { c1 = cs;               continue; }
			    c2 = ( cs != c1 ) ? cs : c2;
			}
		    }
		    out.write("\t" + ((c1 != NOC1 && c2 != NOC2 && c1 != c2) ? "1" : "0"));
		    out.write("\t" + ((cod1[c] != 0 && cod2[c] == 0) ? lblCDS.get(idCDS1[c]) : (cod1[c] == 0 && cod2[c] != 0) ? lblCDS.get(idCDS2[c]) : lblCDS.get(idCDS1[c])+"/"+lblCDS.get(idCDS2[c])));
		}
		out.newLine();
	    }
	    
	}
	out.close();
	if ( verb ) {
	    System.err.println("variable characters:      " + basename + ".var.tsv"); 
	}
	
	
	
	//##########################################################################################################
	//### writing MSA                                                                                        ###
	//##########################################################################################################
	out = new BufferedWriter(new FileWriter(new File(basename + ".fasta")));
	i = -1;
	while ( ++i < n ) {
	    out.write(">" + fh.get(i));
	    out.newLine();
	    /*
	      sb = new StringBuilder(msa[i]);
	      c = lref; while ( --c >= 0 ) if ( ! keep.get(c) ) sb.setCharAt(c, DELCHAR);
	      //out.write(sb.toString().replaceAll(String.valueOf(DELCHAR), ""));
	      out.write(CANCELER.matcher(sb.toString()).replaceAll(""));
	    */
	    out.write(cleanSeq(msa[i], keep));
	    out.newLine();
	}
	out.close();
	if ( verb ) {
	    System.err.println("output MSA:               " + basename + ".fasta"); 
	}

    } 


    //##### deletes characters from 'seq' that are associated to false in the BitSet 'keep'
    final static String cleanSeq(final String seq, final BitSet keep) {
	StringBuilder sb = new StringBuilder(seq.length());
	int c = -1;
	while ( (c=keep.nextSetBit(++c)) >= 0 ) sb = sb.append(seq.charAt(c));
	return sb.toString();
    }

    //##### quickly splits a GFF3 tsv line
    final static String[] gff3Split(final String row) {
	String[] split = new String[9];
	int x = 0, y = row.indexOf('\t');    split[0] = row.substring(x, y);                              //## NOTE: sequence label
	y = row.indexOf('\t', (x=++y));   // split[1] = row.substring(x, y);                              //## NOTE: source            => not used
	y = row.indexOf('\t', (x=++y));      split[2] = row.substring(x, y);                              //## NOTE: feature, e.g. CDS
	y = row.indexOf('\t', (x=++y));      split[3] = row.substring(x, y);                              //## NOTE: starting position
	y = row.indexOf('\t', (x=++y));      split[4] = row.substring(x, y);                              //## NOTE: ending position
	y = row.indexOf('\t', (x=++y));   // split[5] = row.substring(x, y);                              //## NOTE: score             => not used
	y = row.indexOf('\t', (x=++y));      split[6] = row.substring(x, y);                              //## NOTE: strand
	y = row.indexOf('\t', (x=++y));      split[7] = row.substring(x, y);                              //## NOTE: phase
	y = row.indexOf('\t', (x=++y));      split[8] = (y < 0) ? row.substring(x) : row.substring(x, y); //## NOTE: annotation info
	return split;
    }

    //##### extracts the codon corresponding to a given position
    //##### coding regions are indicated by cod1 and cod2
    final static String extractCodon(final int pos, final String seq, final byte[] cod1, final byte[] cod2) {
	String codon = "";
	if ( cod1[pos] != 0 ) {
	    switch ( cod1[pos] ) {
	    case B1:  codon =    seq.substring(c,   c+3);  break;
	    case B2:  codon =    seq.substring(c-1, c+2);  break;
	    case B3:  codon =    seq.substring(c-2, c+1);  break;
	    case B_1: codon = rc(seq.substring(c-2, c+1)); break;
	    case B_2: codon = rc(seq.substring(c-1, c+2)); break;
	    case B_3: codon = rc(seq.substring(c,   c+3)); break;
	    }
	    if ( cod2[pos] != 0 ) codon = codon + "/";
	}
	if ( cod2[pos] != 0 ) {
	    switch ( cod2[pos] ) {
	    case B1:  codon = codon +    seq.substring(c,   c+3);  break;
	    case B2:  codon = codon +    seq.substring(c-1, c+2);  break;
	    case B3:  codon = codon +    seq.substring(c-2, c+1);  break;
	    case B_1: codon = codon + rc(seq.substring(c-2, c+1)); break;
	    case B_2: codon = codon + rc(seq.substring(c-1, c+2)); break;
	    case B_3: codon = codon + rc(seq.substring(c,   c+3)); break;
	    }
	}
	return codon;
    }
    
    
    //##### reverse-complements a codon
    final static String rc(final String codon) {
        StringBuilder rc = new StringBuilder("");
	int c = 3;
	while ( --c >= 0 )
	    switch ( codon.charAt(c) ) {
	    case 'A': rc = rc.append("T"); break;
	    case 'C': rc = rc.append("G"); break;
	    case 'G': rc = rc.append("C"); break;
	    case 'T': rc = rc.append("A"); break;
	    case 'M': rc = rc.append("K"); break;
	    case 'R': rc = rc.append("Y"); break;
	    case 'W': rc = rc.append("W"); break;
	    case 'S': rc = rc.append("S"); break;
	    case 'Y': rc = rc.append("R"); break;
	    case 'K': rc = rc.append("M"); break;
	    case 'B': rc = rc.append("V"); break;
	    case 'D': rc = rc.append("H"); break;
	    case 'H': rc = rc.append("D"); break;
	    case 'V': rc = rc.append("B"); break;
	    case 'N': rc = rc.append("N"); break;
	    case 'X': rc = rc.append("X"); break;
	    case 'a': rc = rc.append("t"); break;
	    case 'c': rc = rc.append("g"); break;
	    case 'g': rc = rc.append("c"); break;
	    case 't': rc = rc.append("a"); break;
	    case 'm': rc = rc.append("k"); break;
	    case 'r': rc = rc.append("y"); break;
	    case 'w': rc = rc.append("w"); break;
	    case 's': rc = rc.append("s"); break;
	    case 'y': rc = rc.append("r"); break;
	    case 'k': rc = rc.append("m"); break;
	    case 'b': rc = rc.append("v"); break;
	    case 'd': rc = rc.append("h"); break;
	    case 'h': rc = rc.append("d"); break;
	    case 'v': rc = rc.append("b"); break;
	    case 'n': rc = rc.append("n"); break;
	    case 'x': rc = rc.append("x"); break;
	    case '-': rc = rc.append("-"); break;
	    default:  rc = rc.append("?"); break;
	    }
        return rc.toString();
    }



    //##### translates a codon
    //##### translation tables are derived from https://www.ncbi.nlm.nih.gov/Taxonomy/Utils/wprintgc.cgi
    final static String[][] CODON_LIST = {{},
					  //## Standard Code (transl_table=1)
					  {"---","AAA","AAC","AAG","AAR","AAT","AAY","ACA","ACB","ACC","ACD","ACG","ACH","ACK","ACM","ACN","ACR","ACS","ACT","ACV","ACW","ACX","ACY","AGA","AGC","AGG","AGR","AGT","AGY","ATA","ATC","ATG","ATH","ATM","ATT","ATW","ATY","CAA","CAC","CAG","CAR","CAT","CAY","CCA","CCB","CCC","CCD","CCG","CCH","CCK","CCM","CCN","CCR","CCS","CCT","CCV","CCW","CCX","CCY","CGA","CGB","CGC","CGD","CGG","CGH","CGK","CGM","CGN","CGR","CGS","CGT","CGV","CGW","CGX","CGY","CTA","CTB","CTC","CTD","CTG","CTH","CTK","CTM","CTN","CTR","CTS","CTT","CTV","CTW","CTX","CTY","GAA","GAC","GAG","GAR","GAT","GAY","GCA","GCB","GCC","GCD","GCG","GCH","GCK","GCM","GCN","GCR","GCS","GCT","GCV","GCW","GCX","GCY","GGA","GGB","GGC","GGD","GGG","GGH","GGK","GGM","GGN","GGR","GGS","GGT","GGV","GGW","GGX","GGY","GTA","GTB","GTC","GTD","GTG","GTH","GTK","GTM","GTN","GTR","GTS","GTT","GTV","GTW","GTX","GTY","HTA","MGA","MGG","MGR","MTA","MTC","MTH","MTM","MTT","MTW","MTY","NNB","NND","NNK","NNN","NNS","NNV","NNX","NXB","NXD","NXK","NXN","NXS","NXV","NXX","RAC","RAT","RAY","SAA","SAG","SAR","TAA","TAC","TAG","TAR","TAT","TAY","TCA","TCB","TCC","TCD","TCG","TCH","TCK","TCM","TCN","TCR","TCS","TCT","TCV","TCW","TCX","TCY","TGA","TGC","TGG","TGT","TGY","TRA","TTA","TTC","TTG","TTR","TTT","TTY","WTA","XNB","XND","XNK","XNN","XNS","XNV","XNX","XXB","XXD","XXK","XXN","XXS","XXV","XXX","YTA","YTG","YTR"},
					  //## Vertebrate Mitochondrial Code (transl_table=2)
					  {"---","AAA","AAC","AAG","AAR","AAT","AAY","ACA","ACB","ACC","ACD","ACG","ACH","ACK","ACM","ACN","ACR","ACS","ACT","ACV","ACW","ACX","ACY","AGA","AGC","AGG","AGR","AGT","AGY","ATA","ATC","ATG","ATR","ATT","ATY","CAA","CAC","CAG","CAR","CAT","CAY","CCA","CCB","CCC","CCD","CCG","CCH","CCK","CCM","CCN","CCR","CCS","CCT","CCV","CCW","CCX","CCY","CGA","CGB","CGC","CGD","CGG","CGH","CGK","CGM","CGN","CGR","CGS","CGT","CGV","CGW","CGX","CGY","CTA","CTB","CTC","CTD","CTG","CTH","CTK","CTM","CTN","CTR","CTS","CTT","CTV","CTW","CTX","CTY","GAA","GAC","GAG","GAR","GAT","GAY","GCA","GCB","GCC","GCD","GCG","GCH","GCK","GCM","GCN","GCR","GCS","GCT","GCV","GCW","GCX","GCY","GGA","GGB","GGC","GGD","GGG","GGH","GGK","GGM","GGN","GGR","GGS","GGT","GGV","GGW","GGX","GGY","GTA","GTB","GTC","GTD","GTG","GTH","GTK","GTM","GTN","GTR","GTS","GTT","GTV","GTW","GTX","GTY","MTC","MTT","MTY","NNB","NND","NNH","NNK","NNM","NNN","NNS","NNV","NNW","NNX","NXB","NXD","NXH","NXK","NXM","NXN","NXS","NXV","NXW","NXX","RAC","RAT","RAY","SAA","SAG","SAR","TAA","TAC","TAG","TAR","TAT","TAY","TCA","TCB","TCC","TCD","TCG","TCH","TCK","TCM","TCN","TCR","TCS","TCT","TCV","TCW","TCX","TCY","TGA","TGC","TGG","TGR","TGT","TGY","TTA","TTC","TTG","TTR","TTT","TTY","XNB","XND","XNH","XNK","XNM","XNN","XNS","XNV","XNW","XNX","XXB","XXD","XXH","XXK","XXM","XXN","XXS","XXV","XXW","XXX","YTA","YTG","YTR"},
					  //## Yeast Mitochondrial Code (transl_table=3)
					  {"---","AAA","AAC","AAG","AAR","AAT","AAY","ACA","ACB","ACC","ACD","ACG","ACH","ACK","ACM","ACN","ACR","ACS","ACT","ACV","ACW","ACX","ACY","AGA","AGC","AGG","AGR","AGT","AGY","ATA","ATC","ATG","ATR","ATT","ATY","CAA","CAC","CAG","CAR","CAT","CAY","CCA","CCB","CCC","CCD","CCG","CCH","CCK","CCM","CCN","CCR","CCS","CCT","CCV","CCW","CCX","CCY","CGA","CGB","CGC","CGD","CGG","CGH","CGK","CGM","CGN","CGR","CGS","CGT","CGV","CGW","CGX","CGY","CTA","CTB","CTC","CTD","CTG","CTH","CTK","CTM","CTN","CTR","CTS","CTT","CTV","CTW","CTX","CTY","GAA","GAC","GAG","GAR","GAT","GAY","GCA","GCB","GCC","GCD","GCG","GCH","GCK","GCM","GCN","GCR","GCS","GCT","GCV","GCW","GCX","GCY","GGA","GGB","GGC","GGD","GGG","GGH","GGK","GGM","GGN","GGR","GGS","GGT","GGV","GGW","GGX","GGY","GTA","GTB","GTC","GTD","GTG","GTH","GTK","GTM","GTN","GTR","GTS","GTT","GTV","GTW","GTX","GTY","MGA","MGG","MGR","NNB","NND","NNH","NNK","NNM","NNN","NNS","NNV","NNW","NNX","NXB","NXD","NXH","NXK","NXM","NXN","NXS","NXV","NXW","NXX","RAC","RAT","RAY","SAA","SAG","SAR","TAA","TAC","TAG","TAR","TAT","TAY","TCA","TCB","TCC","TCD","TCG","TCH","TCK","TCM","TCN","TCR","TCS","TCT","TCV","TCW"},
					  //## Mold, Protozoan, and Coelenterate Mitochondrial Code and the Mycoplasma/Spiroplasma Code (transl_table=4)
					  {"---","AAA","AAC","AAG","AAR","AAT","AAY","ACA","ACB","ACC","ACD","ACG","ACH","ACK","ACM","ACN","ACR","ACS","ACT","ACV","ACW","ACX","ACY","AGA","AGC","AGG","AGR","AGT","AGY","ATA","ATC","ATG","ATH","ATM","ATT","ATW","ATY","CAA","CAC","CAG","CAR","CAT","CAY","CCA","CCB","CCC","CCD","CCG","CCH","CCK","CCM","CCN","CCR","CCS","CCT","CCV","CCW","CCX","CCY","CGA","CGB","CGC","CGD","CGG","CGH","CGK","CGM","CGN","CGR","CGS","CGT","CGV","CGW","CGX","CGY","CTA","CTB","CTC","CTD","CTG","CTH","CTK","CTM","CTN","CTR","CTS","CTT","CTV","CTW","CTX","CTY","GAA","GAC","GAG","GAR","GAT","GAY","GCA","GCB","GCC","GCD","GCG","GCH","GCK","GCM","GCN","GCR","GCS","GCT","GCV","GCW","GCX","GCY","GGA","GGB","GGC","GGD","GGG","GGH","GGK","GGM","GGN","GGR","GGS","GGT","GGV","GGW","GGX","GGY","GTA","GTB","GTC","GTD","GTG","GTH","GTK","GTM","GTN","GTR","GTS","GTT","GTV","GTW","GTX","GTY","HTA","MGA","MGG","MGR","MTA","MTC","MTH","MTM","MTT","MTW","MTY","NNB","NND","NNH","NNK","NNM","NNN","NNS","NNV","NNW","NNX","NXB","NXD","NXH","NXK","NXM","NXN","NXS","NXV","NXW","NXX","RAC","RAT","RAY","SAA","SAG","SAR","TAA","TAC","TAG","TAR","TAT","TAY","TCA","TCB","TCC","TCD","TCG","TCH","TCK","TCM","TCN","TCR","TCS","TCT","TCV","TCW","TCX","TCY","TGA","TGC","TGG","TGR","TGT","TGY","TTA","TTC","TTG","TTR","TTT","TTY","WTA","XNB","XND","XNH","XNK","XNM","XNN","XNS","XNV","XNW","XNX","XXB","XXD","XXH","XXK","XXM","XXN","XXS","XXV","XXW","XXX","YTA","YTG","YTR"},
					  //## Invertebrate Mitochondrial Code (transl_table=5)
					  {"---","AAA","AAC","AAG","AAR","AAT","AAY","ACA","ACB","ACC","ACD","ACG","ACH","ACK","ACM","ACN","ACR","ACS","ACT","ACV","ACW","ACX","ACY","AGA","AGB","AGC","AGD","AGG","AGH","AGK","AGM","AGN","AGR","AGS","AGT","AGV","AGW","AGX","AGY","ATA","ATC","ATG","ATR","ATT","ATY","CAA","CAC","CAG","CAR","CAT","CAY","CCA","CCB","CCC","CCD","CCG","CCH","CCK","CCM","CCN","CCR","CCS","CCT","CCV","CCW","CCX","CCY","CGA","CGB","CGC","CGD","CGG","CGH","CGK","CGM","CGN","CGR","CGS","CGT","CGV","CGW","CGX","CGY","CTA","CTB","CTC","CTD","CTG","CTH","CTK","CTM","CTN","CTR","CTS","CTT","CTV","CTW","CTX","CTY","GAA","GAC","GAG","GAR","GAT","GAY","GCA","GCB","GCC","GCD","GCG","GCH","GCK","GCM","GCN","GCR","GCS","GCT","GCV","GCW","GCX","GCY","GGA","GGB","GGC","GGD","GGG","GGH","GGK","GGM","GGN","GGR","GGS","GGT","GGV","GGW","GGX","GGY","GTA","GTB","GTC","GTD","GTG","GTH","GTK","GTM","GTN","GTR","GTS","GTT","GTV","GTW","GTX","GTY","MTC","MTT","MTY","NNB","NND","NNH","NNK","NNM","NNN","NNS","NNV","NNW","NNX","NXB","NXD","NXH","NXK","NXM","NXN","NXS","NXV","NXW","NXX","RAC","RAT","RAY","SAA","SAG","SAR","TAA","TAC","TAG","TAR","TAT","TAY","TCA","TCB","TCC","TCD","TCG","TCH","TCK","TCM","TCN","TCR","TCS","TCT","TCV","TCW","TCX","TCY","TGA","TGC","TGG","TGR","TGT","TGY","TTA","TTC","TTG","TTR","TTT","TTY","XNB","XND","XNH","XNK","XNM","XNN","XNS","XNV","XNW","XNX","XXB","XXD","XXH","XXK","XXM","XXN","XXS","XXV","XXW","XXX","YTA","YTG","YTR"},
					  //## Ciliate, Dasycladacean and Hexamita Nuclear Code (transl_table=6)
					  {"---","AAA","AAC","AAG","AAR","AAT","AAY","ACA","ACB","ACC","ACD","ACG","ACH","ACK","ACM","ACN","ACR","ACS","ACT","ACV","ACW","ACX","ACY","AGA","AGC","AGG","AGR","AGT","AGY","ATA","ATC","ATG","ATH","ATM","ATT","ATW","ATY","BAA","BAG","BAR","CAA","CAC","CAG","CAR","CAT","CAY","CCA","CCB","CCC","CCD","CCG","CCH","CCK","CCM","CCN","CCR","CCS","CCT","CCV","CCW","CCX","CCY","CGA","CGB","CGC","CGD","CGG","CGH","CGK","CGM","CGN","CGR","CGS","CGT","CGV","CGW","CGX","CGY","CTA","CTB","CTC","CTD","CTG","CTH","CTK","CTM","CTN","CTR","CTS","CTT","CTV","CTW","CTX","CTY","GAA","GAC","GAG","GAR","GAT","GAY","GCA","GCB","GCC","GCD","GCG","GCH","GCK","GCM","GCN","GCR","GCS","GCT","GCV","GCW","GCX","GCY","GGA","GGB","GGC","GGD","GGG","GGH","GGK","GGM","GGN","GGR","GGS","GGT","GGV","GGW","GGX","GGY","GTA","GTB","GTC","GTD","GTG","GTH","GTK","GTM","GTN","GTR","GTS","GTT","GTV","GTW","GTX","GTY","HTA","KAA","KAG","KAR","MGA","MGG","MGR","MTA","MTC","MTH","MTM","MTT","MTW","MTY","NNB","NND","NNK","NNN","NNS","NNV","NNX","NXB","NXD","NXK","NXN","NXS","NXV","NXX","RAC","RAT","RAY","SAA","SAG","SAR","TAA","TAC","TAG","TAR","TAT","TAY","TCA","TCB","TCC","TCD","TCG","TCH","TCK","TCM","TCN","TCR","TCS","TCT","TCV","TCW","TCX","TCY","TGA","TGC","TGG","TGT","TGY","TTA","TTC","TTG","TTR","TTT","TTY","WTA","XNB","XND","XNK","XNN","XNS","XNV","XNX","XXB","XXD","XXK","XXN","XXS","XXV","XXX","YAA","YAG","YAR","YTA","YTG","YTR"},
					  //##
					  {},
					  //##
					  {},
					  //## Echinoderm and Flatworm Mitochondrial Code (transl_table=9)
					  {"---","AAA","AAC","AAG","AAH","AAM","AAT","AAW","AAY","ACA","ACB","ACC","ACD","ACG","ACH","ACK","ACM","ACN","ACR","ACS","ACT","ACV","ACW","ACX","ACY","AGA","AGB","AGC","AGD","AGG","AGH","AGK","AGM","AGN","AGR","AGS","AGT","AGV","AGW","AGX","AGY","ATA","ATC","ATG","ATH","ATM","ATT","ATW","ATY","CAA","CAC","CAG","CAR","CAT","CAY","CCA","CCB","CCC","CCD","CCG","CCH","CCK","CCM","CCN","CCR","CCS","CCT","CCV","CCW","CCX","CCY","CGA","CGB","CGC","CGD","CGG","CGH","CGK","CGM","CGN","CGR","CGS","CGT","CGV","CGW","CGX","CGY","CTA","CTB","CTC","CTD","CTG","CTH","CTK","CTM","CTN","CTR","CTS","CTT","CTV","CTW","CTX","CTY","GAA","GAC","GAG","GAR","GAT","GAY","GCA","GCB","GCC","GCD","GCG","GCH","GCK","GCM","GCN","GCR","GCS","GCT","GCV","GCW","GCX","GCY","GGA","GGB","GGC","GGD","GGG","GGH","GGK","GGM","GGN","GGR","GGS","GGT","GGV","GGW","GGX","GGY","GTA","GTB","GTC","GTD","GTG","GTH","GTK","GTM","GTN","GTR","GTS","GTT","GTV","GTW","GTX","GTY","HTA","MTA","MTC","MTH","MTM","MTT","MTW","MTY","NNB","NND","NNK","NNN","NNS","NNV","NNX","NXB","NXD","NXK","NXN","NXS","NXV","NXX","RAC","RAT","RAY","SAA","SAG","SAR","TAA","TAC","TAG","TAR","TAT","TAY","TCA","TCB","TCC","TCD","TCG","TCH","TCK","TCM","TCN","TCR","TCS","TCT","TCV","TCW","TCX","TCY","TGA","TGC","TGG","TGR","TGT","TGY","TTA","TTC","TTG","TTR","TTT","TTY","WTA","XNB","XND","XNK","XNN","XNS","XNV","XNX","XXB","XXD","XXK","XXN","XXS","XXV","XXX","YTA","YTG","YTR"},
					  //## Euplotid Nuclear Code (transl_table=10)
					  {"---","AAA","AAC","AAG","AAR","AAT","AAY","ACA","ACB","ACC","ACD","ACG","ACH","ACK","ACM","ACN","ACR","ACS","ACT","ACV","ACW","ACX","ACY","AGA","AGC","AGG","AGR","AGT","AGY","ATA","ATC","ATG","ATH","ATM","ATT","ATW","ATY","CAA","CAC","CAG","CAR","CAT","CAY","CCA","CCB","CCC","CCD","CCG","CCH","CCK","CCM","CCN","CCR","CCS","CCT","CCV","CCW","CCX","CCY","CGA","CGB","CGC","CGD","CGG","CGH","CGK","CGM","CGN","CGR","CGS","CGT","CGV","CGW","CGX","CGY","CTA","CTB","CTC","CTD","CTG","CTH","CTK","CTM","CTN","CTR","CTS","CTT","CTV","CTW","CTX","CTY","GAA","GAC","GAG","GAR","GAT","GAY","GCA","GCB","GCC","GCD","GCG","GCH","GCK","GCM","GCN","GCR","GCS","GCT","GCV","GCW","GCX","GCY","GGA","GGB","GGC","GGD","GGG","GGH","GGK","GGM","GGN","GGR","GGS","GGT","GGV","GGW","GGX","GGY","GTA","GTB","GTC","GTD","GTG","GTH","GTK","GTM","GTN","GTR","GTS","GTT","GTV","GTW","GTX","GTY","HTA","MGA","MGG","MGR","MTA","MTC","MTH","MTM","MTT","MTW","MTY","NNB","NND","NNK","NNN","NNS","NNV","NNX","NXB","NXD","NXK","NXN","NXS","NXV","NXX","RAC","RAT","RAY","SAA","SAG","SAR","TAA","TAC","TAG","TAR","TAT","TAY","TCA","TCB","TCC","TCD","TCG","TCH","TCK","TCM","TCN","TCR","TCS","TCT","TCV","TCW","TCX","TCY","TGA","TGC","TGG","TGH","TGM","TGT","TGW","TGY","TTA","TTC","TTG","TTR","TTT","TTY","WTA","XNB","XND","XNK","XNN","XNS","XNV","XNX","XXB","XXD","XXK","XXN","XXS","XXV","XXX","YTA","YTG","YTR"},
					  //## Bacterial, Archaeal and Plant Plastid Code (transl_table=11)
					  {"---","AAA","AAC","AAG","AAR","AAT","AAY","ACA","ACB","ACC","ACD","ACG","ACH","ACK","ACM","ACN","ACR","ACS","ACT","ACV","ACW","ACX","ACY","AGA","AGC","AGG","AGR","AGT","AGY","ATA","ATC","ATG","ATH","ATM","ATT","ATW","ATY","CAA","CAC","CAG","CAR","CAT","CAY","CCA","CCB","CCC","CCD","CCG","CCH","CCK","CCM","CCN","CCR","CCS","CCT","CCV","CCW","CCX","CCY","CGA","CGB","CGC","CGD","CGG","CGH","CGK","CGM","CGN","CGR","CGS","CGT","CGV","CGW","CGX","CGY","CTA","CTB","CTC","CTD","CTG","CTH","CTK","CTM","CTN","CTR","CTS","CTT","CTV","CTW","CTX","CTY","GAA","GAC","GAG","GAR","GAT","GAY","GCA","GCB","GCC","GCD","GCG","GCH","GCK","GCM","GCN","GCR","GCS","GCT","GCV","GCW","GCX","GCY","GGA","GGB","GGC","GGD","GGG","GGH","GGK","GGM","GGN","GGR","GGS","GGT","GGV","GGW","GGX","GGY","GTA","GTB","GTC","GTD","GTG","GTH","GTK","GTM","GTN","GTR","GTS","GTT","GTV","GTW","GTX","GTY","HTA","MGA","MGG","MGR","MTA","MTC","MTH","MTM","MTT","MTW","MTY","NNB","NND","NNK","NNN","NNS","NNV","NNX","NXB","NXD","NXK","NXN","NXS","NXV","NXX","RAC","RAT","RAY","SAA","SAG","SAR","TAA","TAC","TAG","TAR","TAT","TAY","TCA","TCB","TCC","TCD","TCG","TCH","TCK","TCM","TCN","TCR","TCS","TCT","TCV","TCW","TCX","TCY","TGA","TGC","TGG","TGT","TGY","TRA","TTA","TTC","TTG","TTR","TTT","TTY","WTA","XNB","XND","XNK","XNN","XNS","XNV","XNX","XXB","XXD","XXK","XXN","XXS","XXV","XXX","YTA","YTG","YTR"},
					  //## Alternative Yeast Nuclear Code (transl_table=12)
					  {"---","AAA","AAC","AAG","AAR","AAT","AAY","ACA","ACB","ACC","ACD","ACG","ACH","ACK","ACM","ACN","ACR","ACS","ACT","ACV","ACW","ACX","ACY","AGA","AGC","AGG","AGR","AGT","AGY","ATA","ATC","ATG","ATH","ATM","ATT","ATW","ATY","CAA","CAC","CAG","CAR","CAT","CAY","CCA","CCB","CCC","CCD","CCG","CCH","CCK","CCM","CCN","CCR","CCS","CCT","CCV","CCW","CCX","CCY","CGA","CGB","CGC","CGD","CGG","CGH","CGK","CGM","CGN","CGR","CGS","CGT","CGV","CGW","CGX","CGY","CTA","CTC","CTG","CTH","CTM","CTT","CTW","CTY","GAA","GAC","GAG","GAR","GAT","GAY","GCA","GCB","GCC","GCD","GCG","GCH","GCK","GCM","GCN","GCR","GCS","GCT","GCV","GCW","GCX","GCY","GGA","GGB","GGC","GGD","GGG","GGH","GGK","GGM","GGN","GGR","GGS","GGT","GGV","GGW","GGX","GGY","GTA","GTB","GTC","GTD","GTG","GTH","GTK","GTM","GTN","GTR","GTS","GTT","GTV","GTW","GTX","GTY","HTA","MGA","MGG","MGR","MTA","MTC","MTH","MTM","MTT","MTW","MTY","NNB","NND","NNK","NNN","NNS","NNV","NNX","NXB","NXD","NXK","NXN","NXS","NXV","NXX","RAC","RAT","RAY","SAA","SAG","SAR","TAA","TAC","TAG","TAR","TAT","TAY","TCA","TCB","TCC","TCD","TCG","TCH","TCK","TCM","TCN","TCR","TCS","TCT","TCV","TCW","TCX","TCY","TGA","TGC","TGG","TGT","TGY","TRA","TTA","TTC","TTG","TTR","TTT","TTY","WTA","XNB","XND","XNK","XNN","XNS","XNV","XNX","XXB","XXD","XXK","XXN","XXS","XXV","XXX","YTA"},
					  //## Ascidian Mitochondrial Code (transl_table=13)
					  {"---","AAA","AAC","AAG","AAR","AAT","AAY","ACA","ACB","ACC","ACD","ACG","ACH","ACK","ACM","ACN","ACR","ACS","ACT","ACV","ACW","ACX","ACY","AGA","AGC","AGG","AGR","AGT","AGY","ATA","ATC","ATG","ATR","ATT","ATY","CAA","CAC","CAG","CAR","CAT","CAY","CCA","CCB","CCC","CCD","CCG","CCH","CCK","CCM","CCN","CCR","CCS","CCT","CCV","CCW","CCX","CCY","CGA","CGB","CGC","CGD","CGG","CGH","CGK","CGM","CGN","CGR","CGS","CGT","CGV","CGW","CGX","CGY","CTA","CTB","CTC","CTD","CTG","CTH","CTK","CTM","CTN","CTR","CTS","CTT","CTV","CTW","CTX","CTY","GAA","GAC","GAG","GAR","GAT","GAY","GCA","GCB","GCC","GCD","GCG","GCH","GCK","GCM","GCN","GCR","GCS","GCT","GCV","GCW","GCX","GCY","GGA","GGB","GGC","GGD","GGG","GGH","GGK","GGM","GGN","GGR","GGS","GGT","GGV","GGW","GGX","GGY","GTA","GTB","GTC","GTD","GTG","GTH","GTK","GTM","GTN","GTR","GTS","GTT","GTV","GTW","GTX","GTY","MTC","MTT","MTY","NNB","NND","NNH","NNK","NNM","NNN","NNS","NNV","NNW","NNX","NXB","NXD","NXH","NXK","NXM","NXN","NXS","NXV","NXW","NXX","RAC","RAT","RAY","RGA","RGG","RGR","SAA","SAG","SAR","TAA","TAC","TAG","TAR","TAT","TAY","TCA","TCB","TCC","TCD","TCG","TCH","TCK","TCM","TCN","TCR","TCS","TCT","TCV","TCW","TCX","TCY","TGA","TGC","TGG","TGR","TGT","TGY","TTA","TTC","TTG","TTR","TTT","TTY","XNB","XND","XNH","XNK","XNM","XNN","XNS","XNV","XNW","XNX","XXB","XXD","XXH","XXK","XXM","XXN","XXS","XXV","XXW","XXX","YTA","YTG","YTR"},
					  //## Alternative Flatworm Mitochondrial Code (transl_table=14)
					  {"---","AAA","AAC","AAG","AAH","AAM","AAT","AAW","AAY","ACA","ACB","ACC","ACD","ACG","ACH","ACK","ACM","ACN","ACR","ACS","ACT","ACV","ACW","ACX","ACY","AGA","AGB","AGC","AGD","AGG","AGH","AGK","AGM","AGN","AGR","AGS","AGT","AGV","AGW","AGX","AGY","ATA","ATC","ATG","ATH","ATM","ATT","ATW","ATY","CAA","CAC","CAG","CAR","CAT","CAY","CCA","CCB","CCC","CCD","CCG","CCH","CCK","CCM","CCN","CCR","CCS","CCT","CCV","CCW","CCX","CCY","CGA","CGB","CGC","CGD","CGG","CGH","CGK","CGM","CGN","CGR","CGS","CGT","CGV","CGW","CGX","CGY","CTA","CTB","CTC","CTD","CTG","CTH","CTK","CTM","CTN","CTR","CTS","CTT","CTV","CTW","CTX","CTY","GAA","GAC","GAG","GAR","GAT","GAY","GCA","GCB","GCC","GCD","GCG","GCH","GCK","GCM","GCN","GCR","GCS","GCT","GCV","GCW","GCX","GCY","GGA","GGB","GGC","GGD","GGG","GGH","GGK","GGM","GGN","GGR","GGS","GGT","GGV","GGW","GGX","GGY","GTA","GTB","GTC","GTD","GTG","GTH","GTK","GTM","GTN","GTR","GTS","GTT","GTV","GTW","GTX","GTY","HTA","MTA","MTC","MTH","MTM","MTT","MTW","MTY","NNB","NND","NNK","NNN","NNS","NNV","NNX","NXB","NXD","NXK","NXN","NXS","NXV","NXX","RAC","RAT","RAY","SAA","SAG","SAR","TAA","TAC","TAG","TAH","TAM","TAT","TAW","TAY","TCA","TCB","TCC","TCD","TCG","TCH","TCK","TCM","TCN","TCR","TCS","TCT","TCV","TCW","TCX","TCY","TGA","TGC","TGG","TGR","TGT","TGY","TTA","TTC","TTG","TTR","TTT","TTY","WTA","XNB","XND","XNK","XNN","XNS","XNV","XNX","XXB","XXD","XXK","XXN","XXS","XXV","XXX","YTA","YTG","YTR"},
					  //##
					  {},
					  //## Chlorophycean Mitochondrial Code (transl_table=16)
					  {"---","AAA","AAC","AAG","AAR","AAT","AAY","ACA","ACB","ACC","ACD","ACG","ACH","ACK","ACM","ACN","ACR","ACS","ACT","ACV","ACW","ACX","ACY","AGA","AGC","AGG","AGR","AGT","AGY","ATA","ATC","ATG","ATH","ATM","ATT","ATW","ATY","CAA","CAC","CAG","CAR","CAT","CAY","CCA","CCB","CCC","CCD","CCG","CCH","CCK","CCM","CCN","CCR","CCS","CCT","CCV","CCW","CCX","CCY","CGA","CGB","CGC","CGD","CGG","CGH","CGK","CGM","CGN","CGR","CGS","CGT","CGV","CGW","CGX","CGY","CTA","CTB","CTC","CTD","CTG","CTH","CTK","CTM","CTN","CTR","CTS","CTT","CTV","CTW","CTX","CTY","GAA","GAC","GAG","GAR","GAT","GAY","GCA","GCB","GCC","GCD","GCG","GCH","GCK","GCM","GCN","GCR","GCS","GCT","GCV","GCW","GCX","GCY","GGA","GGB","GGC","GGD","GGG","GGH","GGK","GGM","GGN","GGR","GGS","GGT","GGV","GGW","GGX","GGY","GTA","GTB","GTC","GTD","GTG","GTH","GTK","GTM","GTN","GTR","GTS","GTT","GTV","GTW","GTX","GTY","HTA","MGA","MGG","MGR","MTA","MTC","MTH","MTM","MTT","MTW","MTY","NNB","NND","NNK","NNN","NNS","NNV","NNX","NXB","NXD","NXK","NXN","NXS","NXV","NXX","RAC","RAT","RAY","SAA","SAG","SAR","TAA","TAC","TAG","TAT","TAY","TCA","TCB","TCC","TCD","TCG","TCH","TCK","TCM","TCN","TCR","TCS","TCT","TCV","TCW","TCX","TCY","TGA","TGC","TGG","TGT","TGY","TRA","TTA","TTC","TTG","TTR","TTT","TTY","TWG","WTA","XNB","XND","XNK","XNN","XNS","XNV","XNX","XXB","XXD","XXK","XXN","XXS","XXV","XXX","YTA","YTG","YTR"},
					  //##
					  {},
					  //##
					  {},
					  //##
					  {},
					  //##
					  {},
					  //## Trematode Mitochondrial Code (transl_table=21)
					  {"---","AAA","AAC","AAG","AAH","AAM","AAT","AAW","AAY","ACA","ACB","ACC","ACD","ACG","ACH","ACK","ACM","ACN","ACR","ACS","ACT","ACV","ACW","ACX","ACY","AGA","AGB","AGC","AGD","AGG","AGH","AGK","AGM","AGN","AGR","AGS","AGT","AGV","AGW","AGX","AGY","ATA","ATC","ATG","ATR","ATT","ATY","CAA","CAC","CAG","CAR","CAT","CAY","CCA","CCB","CCC","CCD","CCG","CCH","CCK","CCM","CCN","CCR","CCS","CCT","CCV","CCW","CCX","CCY","CGA","CGB","CGC","CGD","CGG","CGH","CGK","CGM","CGN","CGR","CGS","CGT","CGV","CGW","CGX","CGY","CTA","CTB","CTC","CTD","CTG","CTH","CTK","CTM","CTN","CTR","CTS","CTT","CTV","CTW","CTX","CTY","GAA","GAC","GAG","GAR","GAT","GAY","GCA","GCB","GCC","GCD","GCG","GCH","GCK","GCM","GCN","GCR","GCS","GCT","GCV","GCW","GCX","GCY","GGA","GGB","GGC","GGD","GGG","GGH","GGK","GGM","GGN","GGR","GGS","GGT","GGV","GGW","GGX","GGY","GTA","GTB","GTC","GTD","GTG","GTH","GTK","GTM","GTN","GTR","GTS","GTT","GTV","GTW","GTX","GTY","MTC","MTT","MTY","NNB","NND","NNH","NNK","NNM","NNN","NNS","NNV","NNW","NNX","NXB","NXD","NXH","NXK","NXM","NXN","NXS","NXV","NXW","NXX","RAC","RAT","RAY","SAA","SAG","SAR","TAA","TAC","TAG","TAR","TAT","TAY","TCA","TCB","TCC","TCD","TCG","TCH","TCK","TCM","TCN","TCR","TCS","TCT","TCV","TCW","TCX","TCY","TGA","TGC","TGG","TGR","TGT","TGY","TTA","TTC","TTG","TTR","TTT","TTY","XNB","XND","XNH","XNK","XNM","XNN","XNS","XNV","XNW","XNX","XXB","XXD","XXH","XXK","XXM","XXN","XXS","XXV","XXW","XXX","YTA","YTG","YTR"},
					  //## Scenedesmus obliquus Mitochondrial Code (transl_table=22)
					  {"---","AAA","AAC","AAG","AAR","AAT","AAY","ACA","ACB","ACC","ACD","ACG","ACH","ACK","ACM","ACN","ACR","ACS","ACT","ACV","ACW","ACX","ACY","AGA","AGC","AGG","AGR","AGT","AGY","ATA","ATC","ATG","ATH","ATM","ATT","ATW","ATY","CAA","CAC","CAG","CAR","CAT","CAY","CCA","CCB","CCC","CCD","CCG","CCH","CCK","CCM","CCN","CCR","CCS","CCT","CCV","CCW","CCX","CCY","CGA","CGB","CGC","CGD","CGG","CGH","CGK","CGM","CGN","CGR","CGS","CGT","CGV","CGW","CGX","CGY","CTA","CTB","CTC","CTD","CTG","CTH","CTK","CTM","CTN","CTR","CTS","CTT","CTV","CTW","CTX","CTY","GAA","GAC","GAG","GAR","GAT","GAY","GCA","GCB","GCC","GCD","GCG","GCH","GCK","GCM","GCN","GCR","GCS","GCT","GCV","GCW","GCX","GCY","GGA","GGB","GGC","GGD","GGG","GGH","GGK","GGM","GGN","GGR","GGS","GGT","GGV","GGW","GGX","GGY","GTA","GTB","GTC","GTD","GTG","GTH","GTK","GTM","GTN","GTR","GTS","GTT","GTV","GTW","GTX","GTY","HTA","MGA","MGG","MGR","MTA","MTC","MTH","MTM","MTT","MTW","MTY","NNB","NND","NNK","NNN","NNS","NNV","NNX","NXB","NXD","NXK","NXN","NXS","NXV","NXX","RAC","RAT","RAY","SAA","SAG","SAR","TAA","TAC","TAG","TAT","TAY","TCA","TCB","TCC","TCG","TCK","TCS","TCT","TCY","TGA","TGC","TGG","TGT","TGY","TMA","TRA","TSA","TTA","TTC","TTG","TTR","TTT","TTY","TVA","TWG","WTA","XNB","XND","XNK","XNN","XNS","XNV","XNX","XXB","XXD","XXK","XXN","XXS","XXV","XXX","YTA","YTG","YTR"},
					  //## Thraustochytrium Mitochondrial Code (transl_table=23)
					  {"---","AAA","AAC","AAG","AAR","AAT","AAY","ACA","ACB","ACC","ACD","ACG","ACH","ACK","ACM","ACN","ACR","ACS","ACT","ACV","ACW","ACX","ACY","AGA","AGC","AGG","AGR","AGT","AGY","ATA","ATC","ATG","ATH","ATM","ATT","ATW","ATY","CAA","CAC","CAG","CAR","CAT","CAY","CCA","CCB","CCC","CCD","CCG","CCH","CCK","CCM","CCN","CCR","CCS","CCT","CCV","CCW","CCX","CCY","CGA","CGB","CGC","CGD","CGG","CGH","CGK","CGM","CGN","CGR","CGS","CGT","CGV","CGW","CGX","CGY","CTA","CTB","CTC","CTD","CTG","CTH","CTK","CTM","CTN","CTR","CTS","CTT","CTV","CTW","CTX","CTY","GAA","GAC","GAG","GAR","GAT","GAY","GCA","GCB","GCC","GCD","GCG","GCH","GCK","GCM","GCN","GCR","GCS","GCT","GCV","GCW","GCX","GCY","GGA","GGB","GGC","GGD","GGG","GGH","GGK","GGM","GGN","GGR","GGS","GGT","GGV","GGW","GGX","GGY","GTA","GTB","GTC","GTD","GTG","GTH","GTK","GTM","GTN","GTR","GTS","GTT","GTV","GTW","GTX","GTY","MGA","MGG","MGR","MTA","MTC","MTH","MTM","MTT","MTW","MTY","NNB","NND","NNK","NNN","NNS","NNV","NNX","NXB","NXD","NXK","NXN","NXS","NXV","NXX","RAC","RAT","RAY","SAA","SAG","SAR","TAA","TAC","TAG","TAR","TAT","TAY","TCA","TCB","TCC","TCD","TCG","TCH","TCK","TCM","TCN","TCR","TCS","TCT","TCV","TCW","TCX","TCY","TDA","TGA","TGC","TGG","TGT","TGY","TKA","TRA","TTA","TTC","TTG","TTT","TTY","TWA","XNB","XND","XNK","XNN","XNS","XNV","XNX","XXB","XXD","XXK","XXN","XXS","XXV","XXX","YTG"},
					  //## Pterobranchia Mitochondrial Code (transl_table=24)
					  {"---","AAA","AAC","AAG","AAR","AAT","AAY","ACA","ACB","ACC","ACD","ACG","ACH","ACK","ACM","ACN","ACR","ACS","ACT","ACV","ACW","ACX","ACY","AGA","AGC","AGG","AGH","AGM","AGT","AGW","AGY","ARG","ATA","ATC","ATG","ATH","ATM","ATT","ATW","ATY","CAA","CAC","CAG","CAR","CAT","CAY","CCA","CCB","CCC","CCD","CCG","CCH","CCK","CCM","CCN","CCR","CCS","CCT","CCV","CCW","CCX","CCY","CGA","CGB","CGC","CGD","CGG","CGH","CGK","CGM","CGN","CGR","CGS","CGT","CGV","CGW","CGX","CGY","CTA","CTB","CTC","CTD","CTG","CTH","CTK","CTM","CTN","CTR","CTS","CTT","CTV","CTW","CTX","CTY","GAA","GAC","GAG","GAR","GAT","GAY","GCA","GCB","GCC","GCD","GCG","GCH","GCK","GCM","GCN","GCR","GCS","GCT","GCV","GCW","GCX","GCY","GGA","GGB","GGC","GGD","GGG","GGH","GGK","GGM","GGN","GGR","GGS","GGT","GGV","GGW","GGX","GGY","GTA","GTB","GTC","GTD","GTG","GTH","GTK","GTM","GTN","GTR","GTS","GTT","GTV","GTW","GTX","GTY","HTA","MTA","MTC","MTH","MTM","MTT","MTW","MTY","NNB","NND","NNH","NNK","NNM","NNN","NNS","NNV","NNW","NNX","NXB","NXD","NXH","NXK","NXM","NXN","NXS","NXV","NXW","NXX","RAC","RAT","RAY","SAA","SAG","SAR","TAA","TAC","TAG","TAR","TAT","TAY","TCA","TCB","TCC","TCD","TCG","TCH","TCK","TCM","TCN","TCR","TCS","TCT","TCV","TCW","TCX","TCY","TGA","TGC","TGG","TGR","TGT","TGY","TTA","TTC","TTG","TTR","TTT","TTY","WTA","XNB","XND","XNH","XNK","XNM","XNN","XNS","XNV","XNW","XNX","XXB","XXD","XXH","XXK","XXM","XXN","XXS","XXV","XXW","XXX","YTA","YTG","YTR"},
					  //## Candidate Division SR1 and Gracilibacteria Code (transl_table=25)
					  {"---","AAA","AAC","AAG","AAR","AAT","AAY","ACA","ACB","ACC","ACD","ACG","ACH","ACK","ACM","ACN","ACR","ACS","ACT","ACV","ACW","ACX","ACY","AGA","AGC","AGG","AGR","AGT","AGY","ATA","ATC","ATG","ATH","ATM","ATT","ATW","ATY","CAA","CAC","CAG","CAR","CAT","CAY","CCA","CCB","CCC","CCD","CCG","CCH","CCK","CCM","CCN","CCR","CCS","CCT","CCV","CCW","CCX","CCY","CGA","CGB","CGC","CGD","CGG","CGH","CGK","CGM","CGN","CGR","CGS","CGT","CGV","CGW","CGX","CGY","CTA","CTB","CTC","CTD","CTG","CTH","CTK","CTM","CTN","CTR","CTS","CTT","CTV","CTW","CTX","CTY","GAA","GAC","GAG","GAR","GAT","GAY","GCA","GCB","GCC","GCD","GCG","GCH","GCK","GCM","GCN","GCR","GCS","GCT","GCV","GCW","GCX","GCY","GGA","GGB","GGC","GGD","GGG","GGH","GGK","GGM","GGN","GGR","GGS","GGT","GGV","GGW","GGX","GGY","GTA","GTB","GTC","GTD","GTG","GTH","GTK","GTM","GTN","GTR","GTS","GTT","GTV","GTW","GTX","GTY","HTA","KGA","MGA","MGG","MGR","MTA","MTC","MTH","MTM","MTT","MTW","MTY","NNB","NND","NNK","NNN","NNS","NNV","NNX","NXB","NXD","NXK","NXN","NXS","NXV","NXX","RAC","RAT","RAY","SAA","SAG","SAR","TAA","TAC","TAG","TAR","TAT","TAY","TCA","TCB","TCC","TCD","TCG","TCH","TCK","TCM","TCN","TCR","TCS","TCT","TCV","TCW","TCX","TCY","TGA","TGC","TGG","TGT","TGY","TTA","TTC","TTG","TTR","TTT","TTY","WTA","XNB","XND","XNK","XNN","XNS","XNV","XNX","XXB","XXD","XXK","XXN","XXS","XXV","XXX","YTA","YTG","YTR"},
					  //## Pachysolen tannophilus Nuclear Code (transl_table=26)
					  {"---","AAA","AAC","AAG","AAR","AAT","AAY","ACA","ACB","ACC","ACD","ACG","ACH","ACK","ACM","ACN","ACR","ACS","ACT","ACV","ACW","ACX","ACY","AGA","AGC","AGG","AGR","AGT","AGY","ATA","ATC","ATG","ATH","ATM","ATT","ATW","ATY","CAA","CAC","CAG","CAR","CAT","CAY","CCA","CCB","CCC","CCD","CCG","CCH","CCK","CCM","CCN","CCR","CCS","CCT","CCV","CCW","CCX","CCY","CGA","CGB","CGC","CGD","CGG","CGH","CGK","CGM","CGN","CGR","CGS","CGT","CGV","CGW","CGX","CGY","CTA","CTC","CTG","CTH","CTM","CTT","CTW","CTY","GAA","GAC","GAG","GAR","GAT","GAY","GCA","GCB","GCC","GCD","GCG","GCH","GCK","GCM","GCN","GCR","GCS","GCT","GCV","GCW","GCX","GCY","GGA","GGB","GGC","GGD","GGG","GGH","GGK","GGM","GGN","GGR","GGS","GGT","GGV","GGW","GGX","GGY","GTA","GTB","GTC","GTD","GTG","GTH","GTK","GTM","GTN","GTR","GTS","GTT","GTV","GTW","GTX","GTY","HTA","MGA","MGG","MGR","MTA","MTC","MTH","MTM","MTT","MTW","MTY","NNB","NND","NNK","NNN","NNS","NNV","NNX","NXB","NXD","NXK","NXN","NXS","NXV","NXX","RAC","RAT","RAY","SAA","SAG","SAR","TAA","TAC","TAG","TAR","TAT","TAY","TCA","TCB","TCC","TCD","TCG","TCH","TCK","TCM","TCN","TCR","TCS","TCT","TCV","TCW","TCX","TCY","TGA","TGC","TGG","TGT","TGY","TRA","TTA","TTC","TTG","TTR","TTT","TTY","WTA","XNB","XND","XNK","XNN","XNS","XNV","XNX","XXB","XXD","XXK","XXN","XXS","XXV","XXX","YTA"},
					  //## Karyorelict Nuclear Code (transl_table=27)
					  {"---","AAA","AAC","AAG","AAR","AAT","AAY","ACA","ACB","ACC","ACD","ACG","ACH","ACK","ACM","ACN","ACR","ACS","ACT","ACV","ACW","ACX","ACY","AGA","AGC","AGG","AGR","AGT","AGY","ATA","ATC","ATG","ATH","ATM","ATT","ATW","ATY","BAA","BAG","BAR","CAA","CAC","CAG","CAR","CAT","CAY","CCA","CCB","CCC","CCD","CCG","CCH","CCK","CCM","CCN","CCR","CCS","CCT","CCV","CCW","CCX","CCY","CGA","CGB","CGC","CGD","CGG","CGH","CGK","CGM","CGN","CGR","CGS","CGT","CGV","CGW","CGX","CGY","CTA","CTB","CTC","CTD","CTG","CTH","CTK","CTM","CTN","CTR","CTS","CTT","CTV","CTW","CTX","CTY","GAA","GAC","GAG","GAR","GAT","GAY","GCA","GCB","GCC","GCD","GCG","GCH","GCK","GCM","GCN","GCR","GCS","GCT","GCV","GCW","GCX","GCY","GGA","GGB","GGC","GGD","GGG","GGH","GGK","GGM","GGN","GGR","GGS","GGT","GGV","GGW","GGX","GGY","GTA","GTB","GTC","GTD","GTG","GTH","GTK","GTM","GTN","GTR","GTS","GTT","GTV","GTW","GTX","GTY","HTA","KAA","KAG","KAR","MGA","MGG","MGR","MTA","MTC","MTH","MTM","MTT","MTW","MTY","NNB","NND","NNK","NNN","NNS","NNV","NNX","NXB","NXD","NXK","NXN","NXS","NXV","NXX","RAC","RAT","RAY","SAA","SAG","SAR","TAA","TAC","TAG","TAR","TAT","TAY","TCA","TCB","TCC","TCD","TCG","TCH","TCK","TCM","TCN","TCR","TCS","TCT","TCV","TCW","TCX","TCY","TGA","TGC","TGG","TGR","TGT","TGY","TTA","TTC","TTG","TTR","TTT","TTY","WTA","XNB","XND","XNK","XNN","XNS","XNV","XNX","XXB","XXD","XXK","XXN","XXS","XXV","XXX","YAA","YAG","YAR","YTA","YTG","YTR"},
					  //## Condylostoma Nuclear Code (transl_table=28)
					  {"---","AAA","AAC","AAG","AAR","AAT","AAY","ACA","ACB","ACC","ACD","ACG","ACH","ACK","ACM","ACN","ACR","ACS","ACT","ACV","ACW","ACX","ACY","AGA","AGC","AGG","AGR","AGT","AGY","ATA","ATC","ATG","ATH","ATM","ATT","ATW","ATY","BAA","BAG","BAR","CAA","CAC","CAG","CAR","CAT","CAY","CCA","CCB","CCC","CCD","CCG","CCH","CCK","CCM","CCN","CCR","CCS","CCT","CCV","CCW","CCX","CCY","CGA","CGB","CGC","CGD","CGG","CGH","CGK","CGM","CGN","CGR","CGS","CGT","CGV","CGW","CGX","CGY","CTA","CTB","CTC","CTD","CTG","CTH","CTK","CTM","CTN","CTR","CTS","CTT","CTV","CTW","CTX","CTY","GAA","GAC","GAG","GAR","GAT","GAY","GCA","GCB","GCC","GCD","GCG","GCH","GCK","GCM","GCN","GCR","GCS","GCT","GCV","GCW","GCX","GCY","GGA","GGB","GGC","GGD","GGG","GGH","GGK","GGM","GGN","GGR","GGS","GGT","GGV","GGW","GGX","GGY","GTA","GTB","GTC","GTD","GTG","GTH","GTK","GTM","GTN","GTR","GTS","GTT","GTV","GTW","GTX","GTY","HTA","KAA","KAG","KAR","MGA","MGG","MGR","MTA","MTC","MTH","MTM","MTT","MTW","MTY","NNB","NND","NNK","NNN","NNS","NNV","NNX","NXB","NXD","NXK","NXN","NXS","NXV","NXX","RAC","RAT","RAY","SAA","SAG","SAR","TAA","TAC","TAG","TAR","TAT","TAY","TCA","TCB","TCC","TCD","TCG","TCH","TCK","TCM","TCN","TCR","TCS","TCT","TCV","TCW","TCX","TCY","TGA","TGC","TGG","TGR","TGT","TGY","TTA","TTC","TTG","TTR","TTT","TTY","WTA","XNB","XND","XNK","XNN","XNS","XNV","XNX","XXB","XXD","XXK","XXN","XXS","XXV","XXX","YAA","YAG","YAR","YTA","YTG","YTR"},
					  //## Mesodinium Nuclear Code (transl_table=29)
					  {"---","AAA","AAC","AAG","AAR","AAT","AAY","ACA","ACB","ACC","ACD","ACG","ACH","ACK","ACM","ACN","ACR","ACS","ACT","ACV","ACW","ACX","ACY","AGA","AGC","AGG","AGR","AGT","AGY","ATA","ATC","ATG","ATH","ATM","ATT","ATW","ATY","CAA","CAC","CAG","CAR","CAT","CAY","CCA","CCB","CCC","CCD","CCG","CCH","CCK","CCM","CCN","CCR","CCS","CCT","CCV","CCW","CCX","CCY","CGA","CGB","CGC","CGD","CGG","CGH","CGK","CGM","CGN","CGR","CGS","CGT","CGV","CGW","CGX","CGY","CTA","CTB","CTC","CTD","CTG","CTH","CTK","CTM","CTN","CTR","CTS","CTT","CTV","CTW","CTX","CTY","GAA","GAC","GAG","GAR","GAT","GAY","GCA","GCB","GCC","GCD","GCG","GCH","GCK","GCM","GCN","GCR","GCS","GCT","GCV","GCW","GCX","GCY","GGA","GGB","GGC","GGD","GGG","GGH","GGK","GGM","GGN","GGR","GGS","GGT","GGV","GGW","GGX","GGY","GTA","GTB","GTC","GTD","GTG","GTH","GTK","GTM","GTN","GTR","GTS","GTT","GTV","GTW","GTX","GTY","HTA","MGA","MGG","MGR","MTA","MTC","MTH","MTM","MTT","MTW","MTY","NNB","NND","NNK","NNN","NNS","NNV","NNX","NXB","NXD","NXK","NXN","NXS","NXV","NXX","RAC","RAT","RAY","SAA","SAG","SAR","TAA","TAB","TAC","TAD","TAG","TAH","TAK","TAM","TAN","TAR","TAS","TAT","TAV","TAW","TAX","TAY","TCA","TCB","TCC","TCD","TCG","TCH","TCK","TCM","TCN","TCR","TCS","TCT","TCV","TCW","TCX","TCY","TGA","TGC","TGG","TGT","TGY","TTA","TTC","TTG","TTR","TTT","TTY","WTA","XNB","XND","XNK","XNN","XNS","XNV","XNX","XXB","XXD","XXK","XXN","XXS","XXV","XXX","YTA","YTG","YTR"},
					  //## Peritrich Nuclear Code (transl_table=30)
					  {"---","AAA","AAC","AAG","AAR","AAT","AAY","ACA","ACB","ACC","ACD","ACG","ACH","ACK","ACM","ACN","ACR","ACS","ACT","ACV","ACW","ACX","ACY","AGA","AGC","AGG","AGR","AGT","AGY","ATA","ATC","ATG","ATH","ATM","ATT","ATW","ATY","BAA","BAG","BAR","CAA","CAC","CAG","CAR","CAT","CAY","CCA","CCB","CCC","CCD","CCG","CCH","CCK","CCM","CCN","CCR","CCS","CCT","CCV","CCW","CCX","CCY","CGA","CGB","CGC","CGD","CGG","CGH","CGK","CGM","CGN","CGR","CGS","CGT","CGV","CGW","CGX","CGY","CTA","CTB","CTC","CTD","CTG","CTH","CTK","CTM","CTN","CTR","CTS","CTT","CTV","CTW","CTX","CTY","GAA","GAC","GAG","GAR","GAT","GAY","GCA","GCB","GCC","GCD","GCG","GCH","GCK","GCM","GCN","GCR","GCS","GCT","GCV","GCW","GCX","GCY","GGA","GGB","GGC","GGD","GGG","GGH","GGK","GGM","GGN","GGR","GGS","GGT","GGV","GGW","GGX","GGY","GTA","GTB","GTC","GTD","GTG","GTH","GTK","GTM","GTN","GTR","GTS","GTT","GTV","GTW","GTX","GTY","HTA","KAA","KAG","KAR","MGA","MGG","MGR","MTA","MTC","MTH","MTM","MTT","MTW","MTY","NNB","NND","NNK","NNN","NNS","NNV","NNX","NXB","NXD","NXK","NXN","NXS","NXV","NXX","RAC","RAT","RAY","SAA","SAG","SAR","TAA","TAC","TAG","TAR","TAT","TAY","TCA","TCB","TCC","TCD","TCG","TCH","TCK","TCM","TCN","TCR","TCS","TCT","TCV","TCW","TCX","TCY","TGA","TGC","TGG","TGT","TGY","TTA","TTC","TTG","TTR","TTT","TTY","WTA","XNB","XND","XNK","XNN","XNS","XNV","XNX","XXB","XXD","XXK","XXN","XXS","XXV","XXX","YAA","YAG","YAR","YTA","YTG","YTR"},
  					  //## Blastocrithidia Nuclear Code (transl_table=31)
					  {"---","AAA","AAC","AAG","AAR","AAT","AAY","ACA","ACB","ACC","ACD","ACG","ACH","ACK","ACM","ACN","ACR","ACS","ACT","ACV","ACW","ACX","ACY","AGA","AGC","AGG","AGR","AGT","AGY","ATA","ATC","ATG","ATH","ATM","ATT","ATW","ATY","BAA","BAG","BAR","CAA","CAC","CAG","CAR","CAT","CAY","CCA","CCB","CCC","CCD","CCG","CCH","CCK","CCM","CCN","CCR","CCS","CCT","CCV","CCW","CCX","CCY","CGA","CGB","CGC","CGD","CGG","CGH","CGK","CGM","CGN","CGR","CGS","CGT","CGV","CGW","CGX","CGY","CTA","CTB","CTC","CTD","CTG","CTH","CTK","CTM","CTN","CTR","CTS","CTT","CTV","CTW","CTX","CTY","GAA","GAC","GAG","GAR","GAT","GAY","GCA","GCB","GCC","GCD","GCG","GCH","GCK","GCM","GCN","GCR","GCS","GCT","GCV","GCW","GCX","GCY","GGA","GGB","GGC","GGD","GGG","GGH","GGK","GGM","GGN","GGR","GGS","GGT","GGV","GGW","GGX","GGY","GTA","GTB","GTC","GTD","GTG","GTH","GTK","GTM","GTN","GTR","GTS","GTT","GTV","GTW","GTX","GTY","HTA","KAA","KAG","KAR","MGA","MGG","MGR","MTA","MTC","MTH","MTM","MTT","MTW","MTY","NNB","NND","NNK","NNN","NNS","NNV","NNX","NXB","NXD","NXK","NXN","NXS","NXV","NXX","RAC","RAT","RAY","SAA","SAG","SAR","TAA","TAC","TAG","TAR","TAT","TAY","TCA","TCB","TCC","TCD","TCG","TCH","TCK","TCM","TCN","TCR","TCS","TCT","TCV","TCW","TCX","TCY","TGA","TGC","TGG","TGR","TGT","TGY","TTA","TTC","TTG","TTR","TTT","TTY","WTA","XNB","XND","XNK","XNN","XNS","XNV","XNX","XXB","XXD","XXK","XXN","XXS","XXV","XXX","YAA","YAG","YAR","YTA","YTG","YTR"},
					  //##
					  {},
					  //## Cephalodiscidae Mitochondrial UAA-Tyr Code (transl_table=33)
					  {"---","AAA","AAC","AAG","AAR","AAT","AAY","ACA","ACB","ACC","ACD","ACG","ACH","ACK","ACM","ACN","ACR","ACS","ACT","ACV","ACW","ACX","ACY","AGA","AGC","AGG","AGH","AGM","AGT","AGW","AGY","ARG","ATA","ATC","ATG","ATH","ATM","ATT","ATW","ATY","CAA","CAC","CAG","CAR","CAT","CAY","CCA","CCB","CCC","CCD","CCG","CCH","CCK","CCM","CCN","CCR","CCS","CCT","CCV","CCW","CCX","CCY","CGA","CGB","CGC","CGD","CGG","CGH","CGK","CGM","CGN","CGR","CGS","CGT","CGV","CGW","CGX","CGY","CTA","CTB","CTC","CTD","CTG","CTH","CTK","CTM","CTN","CTR","CTS","CTT","CTV","CTW","CTX","CTY","GAA","GAC","GAG","GAR","GAT","GAY","GCA","GCB","GCC","GCD","GCG","GCH","GCK","GCM","GCN","GCR","GCS","GCT","GCV","GCW","GCX","GCY","GGA","GGB","GGC","GGD","GGG","GGH","GGK","GGM","GGN","GGR","GGS","GGT","GGV","GGW","GGX","GGY","GTA","GTB","GTC","GTD","GTG","GTH","GTK","GTM","GTN","GTR","GTS","GTT","GTV","GTW","GTX","GTY","HTA","MTA","MTC","MTH","MTM","MTT","MTW","MTY","NNB","NND","NNK","NNN","NNS","NNV","NNX","NXB","NXD","NXK","NXN","NXS","NXV","NXX","RAC","RAT","RAY","SAA","SAG","SAR","TAA","TAC","TAG","TAH","TAM","TAT","TAW","TAY","TCA","TCB","TCC","TCD","TCG","TCH","TCK","TCM","TCN","TCR","TCS","TCT","TCV","TCW","TCX","TCY","TGA","TGC","TGG","TGR","TGT","TGY","TTA","TTC","TTG","TTR","TTT","TTY","WTA","XNB","XND","XNK","XNN","XNS","XNV","XNX","XXB","XXD","XXK","XXN","XXS","XXV","XXX","YTA","YTG","YTR"} };
    final static String[] AA_LIST = {"",
				     //## Standard Code (transl_table=1)
				     "-KNKKNNTTTTTTTTTTTTTTTTRSRRSSIIMIIIIIQHQQHHPPPPPPPPPPPPPPPPRRRRRRRRRRRRRRRRLLLLLLLLLLLLLLLLEDEEDDAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGVVVVVVVVVVVVVVVVJRRRJJJJJJJXXXXXXXXXXXXXXBBBZZZ*Y**YYSSSSSSSSSSSSSSSS*CWCC*LFLLFFJXXXXXXXXXXXXXXLLL",
				     //## Vertebrate Mitochondrial Code (transl_table=2)
				     "-KNKKNNTTTTTTTTTTTTTTTT*S**SSMIMMIIQHQQHHPPPPPPPPPPPPPPPPRRRRRRRRRRRRRRRRLLLLLLLLLLLLLLLLEDEEDDAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGVVVVVVVVVVVVVVVVJJJXXXXXXXXXXXXXXXXXXXXBBBZZZ*Y**YYSSSSSSSSSSSSSSSSWCWWCCLFLLFFXXXXXXXXXXXXXXXXXXXXLLL",
				     //## Yeast Mitochondrial Code (transl_table=3)
				     "-KNKKNNTTTTTTTTTTTTTTTTRSRRSSMIMMIIQHQQHHPPPPPPPPPPPPPPPPRRRRRRRRRRRRRRRRTTTTTTTTTTTTTTTTEDEEDDAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGVVVVVVVVVVVVVVVVRRRXXXXXXXXXXXXXXXXXXXXBBBZZZ*Y**YYSSSSSSSSSSSSSSSSWCWWCCLFLLFFXXXXXXXXXXXXXXXXXXXX",
				     //## Mold, Protozoan, and Coelenterate Mitochondrial Code and the Mycoplasma/Spiroplasma Code (transl_table=4)
				     "-KNKKNNTTTTTTTTTTTTTTTTRSRRSSIIMIIIIIQHQQHHPPPPPPPPPPPPPPPPRRRRRRRRRRRRRRRRLLLLLLLLLLLLLLLLEDEEDDAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGVVVVVVVVVVVVVVVVJRRRJJJJJJJXXXXXXXXXXXXXXXXXXXXBBBZZZ*Y**YYSSSSSSSSSSSSSSSSWCWWCCLFLLFFJXXXXXXXXXXXXXXXXXXXXLLL",
				     //## Invertebrate Mitochondrial Code (transl_table=5)
				     "-KNKKNNTTTTTTTTTTTTTTTTSSSSSSSSSSSSSSSSMIMMIIQHQQHHPPPPPPPPPPPPPPPPRRRRRRRRRRRRRRRRLLLLLLLLLLLLLLLLEDEEDDAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGVVVVVVVVVVVVVVVVJJJXXXXXXXXXXXXXXXXXXXXBBBZZZ*Y**YYSSSSSSSSSSSSSSSSWCWWCCLFLLFFXXXXXXXXXXXXXXXXXXXXLLL",
				     //## Ciliate, Dasycladacean and Hexamita Nuclear Code (transl_table=6)
				     "-KNKKNNTTTTTTTTTTTTTTTTRSRRSSIIMIIIIIZZZQHQQHHPPPPPPPPPPPPPPPPRRRRRRRRRRRRRRRRLLLLLLLLLLLLLLLLEDEEDDAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGVVVVVVVVVVVVVVVVJZZZRRRJJJJJJJXXXXXXXXXXXXXXBBBZZZQYQQYYSSSSSSSSSSSSSSSS*CWCCLFLLFFJXXXXXXXXXXXXXXQQQLLL",
				     //##
				     "",
				     //##
				     "",
				     //## Echinoderm and Flatworm Mitochondrial Code (transl_table=9)
				     "-NNKNNNNNTTTTTTTTTTTTTTTTSSSSSSSSSSSSSSSSIIMIIIIIQHQQHHPPPPPPPPPPPPPPPPRRRRRRRRRRRRRRRRLLLLLLLLLLLLLLLLEDEEDDAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGVVVVVVVVVVVVVVVVJJJJJJJJXXXXXXXXXXXXXXBBBZZZ*Y**YYSSSSSSSSSSSSSSSSWCWWCCLFLLFFJXXXXXXXXXXXXXXLLL",
				     //## Euplotid Nuclear Code (transl_table=10)
				     "-KNKKNNTTTTTTTTTTTTTTTTRSRRSSIIMIIIIIQHQQHHPPPPPPPPPPPPPPPPRRRRRRRRRRRRRRRRLLLLLLLLLLLLLLLLEDEEDDAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGVVVVVVVVVVVVVVVVJRRRJJJJJJJXXXXXXXXXXXXXXBBBZZZ*Y**YYSSSSSSSSSSSSSSSSCCWCCCCCLFLLFFJXXXXXXXXXXXXXXLLL",
				     //## Bacterial, Archaeal and Plant Plastid Code (transl_table=11)
				     "-KNKKNNTTTTTTTTTTTTTTTTRSRRSSIIMIIIIIQHQQHHPPPPPPPPPPPPPPPPRRRRRRRRRRRRRRRRLLLLLLLLLLLLLLLLEDEEDDAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGVVVVVVVVVVVVVVVVJRRRJJJJJJJXXXXXXXXXXXXXXBBBZZZ*Y**YYSSSSSSSSSSSSSSSS*CWCC*LFLLFFJXXXXXXXXXXXXXXLLL",
				     //## Alternative Yeast Nuclear Code (transl_table=12)
				     "-KNKKNNTTTTTTTTTTTTTTTTRSRRSSIIMIIIIIQHQQHHPPPPPPPPPPPPPPPPRRRRRRRRRRRRRRRRLLSLLLLLEDEEDDAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGVVVVVVVVVVVVVVVVJRRRJJJJJJJXXXXXXXXXXXXXXBBBZZZ*Y**YYSSSSSSSSSSSSSSSS*CWCC*LFLLFFJXXXXXXXXXXXXXXL",
				     //## Ascidian Mitochondrial Code (transl_table=13)
				     "-KNKKNNTTTTTTTTTTTTTTTTGSGGSSMIMMIIQHQQHHPPPPPPPPPPPPPPPPRRRRRRRRRRRRRRRRLLLLLLLLLLLLLLLLEDEEDDAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGVVVVVVVVVVVVVVVVJJJXXXXXXXXXXXXXXXXXXXXBBBGGGZZZ*Y**YYSSSSSSSSSSSSSSSSWCWWCCLFLLFFXXXXXXXXXXXXXXXXXXXXLLL",
				     //## Alternative Flatworm Mitochondrial Code (transl_table=14)
				     "-NNKNNNNNTTTTTTTTTTTTTTTTSSSSSSSSSSSSSSSSIIMIIIIIQHQQHHPPPPPPPPPPPPPPPPRRRRRRRRRRRRRRRRLLLLLLLLLLLLLLLLEDEEDDAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGVVVVVVVVVVVVVVVVJJJJJJJJXXXXXXXXXXXXXXBBBZZZYY*YYYYYSSSSSSSSSSSSSSSSWCWWCCLFLLFFJXXXXXXXXXXXXXXLLL",
				     //##
				     "",
				     //## Chlorophycean Mitochondrial Code (transl_table=16)
				     "-KNKKNNTTTTTTTTTTTTTTTTRSRRSSIIMIIIIIQHQQHHPPPPPPPPPPPPPPPPRRRRRRRRRRRRRRRRLLLLLLLLLLLLLLLLEDEEDDAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGVVVVVVVVVVVVVVVVJRRRJJJJJJJXXXXXXXXXXXXXXBBBZZZ*YLYYSSSSSSSSSSSSSSSS*CWCC*LFLLFFLJXXXXXXXXXXXXXXLLL",
				     //##
				     "",
				     //##
				     "",
				     //##
				     "",
				     //##
				     "",
				     //## Trematode Mitochondrial Code (transl_table=21)
				     "-NNKNNNNNTTTTTTTTTTTTTTTTSSSSSSSSSSSSSSSSMIMMIIQHQQHHPPPPPPPPPPPPPPPPRRRRRRRRRRRRRRRRLLLLLLLLLLLLLLLLEDEEDDAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGVVVVVVVVVVVVVVVVJJJXXXXXXXXXXXXXXXXXXXXBBBZZZ*Y**YYSSSSSSSSSSSSSSSSWCWWCCLFLLFFXXXXXXXXXXXXXXXXXXXXLLL",
				     //## Scenedesmus obliquus Mitochondrial Code (transl_table=22)
				     "-KNKKNNTTTTTTTTTTTTTTTTRSRRSSIIMIIIIIQHQQHHPPPPPPPPPPPPPPPPRRRRRRRRRRRRRRRRLLLLLLLLLLLLLLLLEDEEDDAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGVVVVVVVVVVVVVVVVJRRRJJJJJJJXXXXXXXXXXXXXXBBBZZZ*YLYY*SSSSSSS*CWCC***LFLLFF*LJXXXXXXXXXXXXXXLLL",
				     //## Thraustochytrium Mitochondrial Code (transl_table=23)
				     "-KNKKNNTTTTTTTTTTTTTTTTRSRRSSIIMIIIIIQHQQHHPPPPPPPPPPPPPPPPRRRRRRRRRRRRRRRRLLLLLLLLLLLLLLLLEDEEDDAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGVVVVVVVVVVVVVVVVRRRJJJJJJJXXXXXXXXXXXXXXBBBZZZ*Y**YYSSSSSSSSSSSSSSSS**CWCC***FLFF*XXXXXXXXXXXXXXL",
				     //## Pterobranchia Mitochondrial Code (transl_table=24)
				     "-KNKKNNTTTTTTTTTTTTTTTTSSKSSSSSKIIMIIIIIQHQQHHPPPPPPPPPPPPPPPPRRRRRRRRRRRRRRRRLLLLLLLLLLLLLLLLEDEEDDAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGVVVVVVVVVVVVVVVVJJJJJJJJXXXXXXXXXXXXXXXXXXXXBBBZZZ*Y**YYSSSSSSSSSSSSSSSSWCWWCCLFLLFFJXXXXXXXXXXXXXXXXXXXXLLL",
				     //## Candidate Division SR1 and Gracilibacteria Code (transl_table=25)
				     "-KNKKNNTTTTTTTTTTTTTTTTRSRRSSIIMIIIIIQHQQHHPPPPPPPPPPPPPPPPRRRRRRRRRRRRRRRRLLLLLLLLLLLLLLLLEDEEDDAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGVVVVVVVVVVVVVVVVJGRRRJJJJJJJXXXXXXXXXXXXXXBBBZZZ*Y**YYSSSSSSSSSSSSSSSSGCWCCLFLLFFJXXXXXXXXXXXXXXLLL",
				     //## Pachysolen tannophilus Nuclear Code (transl_table=26)
				     "-KNKKNNTTTTTTTTTTTTTTTTRSRRSSIIMIIIIIQHQQHHPPPPPPPPPPPPPPPPRRRRRRRRRRRRRRRRLLALLLLLEDEEDDAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGVVVVVVVVVVVVVVVVJRRRJJJJJJJXXXXXXXXXXXXXXBBBZZZ*Y**YYSSSSSSSSSSSSSSSS*CWCC*LFLLFFJXXXXXXXXXXXXXXL",
				     //## Karyorelict Nuclear Code (transl_table=27)
				     "-KNKKNNTTTTTTTTTTTTTTTTRSRRSSIIMIIIIIZZZQHQQHHPPPPPPPPPPPPPPPPRRRRRRRRRRRRRRRRLLLLLLLLLLLLLLLLEDEEDDAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGVVVVVVVVVVVVVVVVJZZZRRRJJJJJJJXXXXXXXXXXXXXXBBBZZZQYQQYYSSSSSSSSSSSSSSSSWCWWCCLFLLFFJXXXXXXXXXXXXXXQQQLLL",
				     //## Condylostoma Nuclear Code (transl_table=28)
				     "-KNKKNNTTTTTTTTTTTTTTTTRSRRSSIIMIIIIIZZZQHQQHHPPPPPPPPPPPPPPPPRRRRRRRRRRRRRRRRLLLLLLLLLLLLLLLLEDEEDDAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGVVVVVVVVVVVVVVVVJZZZRRRJJJJJJJXXXXXXXXXXXXXXBBBZZZQYQQYYSSSSSSSSSSSSSSSSWCWWCCLFLLFFJXXXXXXXXXXXXXXQQQLLL",
				     //## Mesodinium Nuclear Code (transl_table=29)
				     "-KNKKNNTTTTTTTTTTTTTTTTRSRRSSIIMIIIIIQHQQHHPPPPPPPPPPPPPPPPRRRRRRRRRRRRRRRRLLLLLLLLLLLLLLLLEDEEDDAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGVVVVVVVVVVVVVVVVJRRRJJJJJJJXXXXXXXXXXXXXXBBBZZZYYYYYYYYYYYYYYYYSSSSSSSSSSSSSSSS*CWCCLFLLFFJXXXXXXXXXXXXXXLLL",
				     //## Peritrich Nuclear Code (transl_table=30)
				     "-KNKKNNTTTTTTTTTTTTTTTTRSRRSSIIMIIIIIZZZQHQQHHPPPPPPPPPPPPPPPPRRRRRRRRRRRRRRRRLLLLLLLLLLLLLLLLEDEEDDAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGVVVVVVVVVVVVVVVVJEEERRRJJJJJJJXXXXXXXXXXXXXXBBBZZZEYEEYYSSSSSSSSSSSSSSSS*CWCCLFLLFFJXXXXXXXXXXXXXXZZZLLL",
				     //## Blastocrithidia Nuclear Code (transl_table=31)
				     "-KNKKNNTTTTTTTTTTTTTTTTRSRRSSIIMIIIIIZZZQHQQHHPPPPPPPPPPPPPPPPRRRRRRRRRRRRRRRRLLLLLLLLLLLLLLLLEDEEDDAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGVVVVVVVVVVVVVVVVJEEERRRJJJJJJJXXXXXXXXXXXXXXBBBZZZEYEEYYSSSSSSSSSSSSSSSSWCWWCCLFLLFFJXXXXXXXXXXXXXXZZZLLL",
				     //##
				     "",
				     //## Cephalodiscidae Mitochondrial UAA-Tyr Code (transl_table=33)
				     "-KNKKNNTTTTTTTTTTTTTTTTSSKSSSSSKIIMIIIIIQHQQHHPPPPPPPPPPPPPPPPRRRRRRRRRRRRRRRRLLLLLLLLLLLLLLLLEDEEDDAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGVVVVVVVVVVVVVVVVJJJJJJJJXXXXXXXXXXXXXXBBBZZZYY*YYYYYSSSSSSSSSSSSSSSSWCWWCCLFLLFFJXXXXXXXXXXXXXXLLL" };
    final static char c2a(final String codon, final int transl_table) {
	if ( transl_table < 1 || transl_table > AA_LIST.length || CODON_LIST[transl_table].length == 0 ) return '?' ;
	int c = Arrays.binarySearch(CODON_LIST[transl_table], codon);
	return ( c < 0 ) ? '?' : AA_LIST[transl_table].charAt(c);
    }

}
